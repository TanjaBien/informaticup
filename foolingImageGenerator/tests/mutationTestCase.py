import unittest
import random
from evol.evolution import Evolution
from enums.parameter import Parameter
from enums.mutation import Mutation
from util.fileManager import FileManager
from enums.dataType import DataType


class MutationTestCase(unittest.TestCase):
    def setUp(self):
        FileManager.MODE = "test"
        self.test_images_path = FileManager.get_relative_path_for_data_type(DataType.TEST_DATA, ["images"])
        self.config = {"mode": "main",
                       "main": []}
        self.metadata = {Parameter.SUCCESSORS_COUNT.get_name(): Parameter.SUCCESSORS_COUNT.get_default_value(),
                         Parameter.ALLOW_DOWNSWING.get_name(): Parameter.ALLOW_DOWNSWING.get_default_value(),
                         Parameter.MAX_GENERATION_NO.get_name(): Parameter.MAX_GENERATION_NO.get_default_value(),
                         Parameter.CONFIDENCE_THRESHOLD.get_name(): Parameter.CONFIDENCE_THRESHOLD.get_default_value(),
                         "parent": {Parameter.PARENT_GRADIENT.get_name(): Parameter.PARENT_GRADIENT.get_default_value(),
                                    Parameter.PARENT_COLUMNS_AND_ROWS.get_name():
                                        Parameter.PARENT_COLUMNS_AND_ROWS.get_default_value(),
                                    Parameter.PARENT_PATH.get_name(): Parameter.PARENT_PATH.get_default_value()},
                         "mutation": {"mode": Parameter.MUTATION_NAME.get_default_value(),
                                      Parameter.COLUMNS_AND_ROWS.get_name():
                                          Parameter.COLUMNS_AND_ROWS.get_default_value(),
                                      Parameter.GRADIENT.get_name(): Parameter.GRADIENT.get_default_value()}}
        self.evolution = Evolution()

    def test_evolution_with_circles(self):
        random.seed(1337)
        parameters = {Parameter.MUTATION_NAME.get_name(): Mutation.DRAW_CIRCLES.value,
                      Parameter.CIRCLES_PER_ROUND.get_name(): 7,
                      Parameter.DIAMETER_TO_IMAGE_SIZE.get_name(): 80
                      }
        metadata = self.metadata
        metadata["mutation"] = {"mode": Mutation.DRAW_CIRCLES.value,
                                Parameter.CIRCLES_PER_ROUND.get_name(): 7,
                                Parameter.DIAMETER_TO_IMAGE_SIZE.get_name(): 80,
                                Parameter.FILLED.get_name(): Parameter.FILLED.get_default_value(),
                                'ratio_max_line_thickness_to_radius': 2}

        self.evolution.start(**parameters)
        self.assertEqual(self.evolution.get_evolution_metadata(), metadata,
                         "incorrect metadata returned from evolution with mutation draw_circles")
        self.assertEqual(self.evolution.is_fooling_image_generated(), True,
                         "no fooling image generated from evolution with mutation draw_circles")

    def test_evolution_with_rectangles(self):
        random.seed(1337)
        parameters = {Parameter.MUTATION_NAME.get_name(): Mutation.DRAW_RECTANGLES.value,
                      Parameter.RECTANGLES_PER_ROUND.get_name(): 2,
                      Parameter.FILLED.get_name(): False
                      }
        metadata = self.metadata
        metadata["mutation"] = {"mode": Mutation.DRAW_RECTANGLES.value,
                                Parameter.RECTANGLES_PER_ROUND.get_name():
                                    parameters[Parameter.RECTANGLES_PER_ROUND.get_name()],
                                Parameter.FILLED.get_name(): parameters[Parameter.FILLED.get_name()],
                                'max_line_thickness_factor': 0.05,}
        self.evolution.start(**parameters)
        self.assertEqual(self.evolution.get_evolution_metadata(), metadata,
                         "incorrect metadata returned from evolution with mutation draw_rectangles")
        self.assertEqual(self.evolution.is_fooling_image_generated(), True,
                         "no fooling image generated from evolution with mutation draw_rectangles")

    def test_evolution_with_grid(self):
        random.seed(1337)
        parameters = {Parameter.MUTATION_NAME.get_name(): Mutation.COLOR_GRID.value,
                      Parameter.COLUMNS_AND_ROWS.get_name(): 2,
                      Parameter.GRADIENT.get_name(): True
                      }
        metadata = self.metadata
        metadata["mutation"] = {"mode": Mutation.COLOR_GRID.value,
                                Parameter.COLUMNS_AND_ROWS.get_name():
                                    parameters[Parameter.COLUMNS_AND_ROWS.get_name()],
                                Parameter.GRADIENT.get_name(): parameters[Parameter.GRADIENT.get_name()]}
        self.evolution.start(**parameters)
        self.assertEqual(self.evolution.get_evolution_metadata(), metadata,
                         "incorrect metadata returned from evolution with mutation color_grid")
        self.assertEqual(self.evolution.is_fooling_image_generated(), True,
                         "no fooling image generated from evolution with mutation color_grid")

    def test_evolution_with_polygons(self):
        random.seed(1337)
        parameters = {Parameter.MUTATION_NAME.get_name(): Mutation.POINT_SYMMETRIC_POLYGONS.value,
                      Parameter.VERTICES.get_name(): 3,
                      Parameter.POLYGONS_PER_ROUND.get_name(): 4,
                      Parameter.AREAS.get_name(): Parameter.AREAS.get_default_value()
                      }
        metadata = self.metadata
        metadata["mutation"] = {"mode": Mutation.POINT_SYMMETRIC_POLYGONS.value,
                                Parameter.VERTICES.get_name(): parameters[Parameter.VERTICES.get_name()],
                                Parameter.POLYGONS_PER_ROUND.get_name():
                                    parameters[Parameter.POLYGONS_PER_ROUND.get_name()],
                                Parameter.AREAS.get_name(): Parameter.AREAS.get_default_value()}
        self.evolution.start(**parameters)
        self.assertEqual(self.evolution.get_evolution_metadata(), metadata,
                         "incorrect metadata returned from evolution with mutation point_symmetric_polygons")
        self.assertEqual(self.evolution.is_fooling_image_generated(), True,
                         "no fooling image generated from evolution with mutation point_symmetric_polygons")

    def test_evolution_with_pixels(self):
        random.seed(1337)
        parameters = {Parameter.MUTATION_NAME.get_name(): Mutation.RANDOM_PIXELS.value,
                      Parameter.PERCENT.get_name(): 5,
                      Parameter.MAX_GENERATION_NO.get_name(): 4
                      }
        metadata = self.metadata
        metadata["mutation"] = {"mode": Mutation.RANDOM_PIXELS.value,
                                Parameter.PERCENT.get_name(): parameters[Parameter.PERCENT.get_name()]}
        metadata[Parameter.MAX_GENERATION_NO.get_name()] = parameters[Parameter.MAX_GENERATION_NO.get_name()]
        self.evolution.start(**parameters)
        self.assertEqual(self.evolution.get_evolution_metadata(), metadata,
                         "incorrect metadata returned from evolution with mutation random_pixels")
        self.assertNotEqual(self.evolution.is_fooling_image_generated(), True,
                            "wrong return value for fooling image generated from evolution with mutation random_pixels")

    def test_evolution_with_parent_options(self):
        random.seed(1337)
        parameters = {Parameter.PARENT_GRADIENT.get_name(): True,
                      Parameter.PARENT_COLUMNS_AND_ROWS.get_name(): 2,
                      Parameter.PARENT_PATH.get_name(): None
                      }
        metadata = self.metadata
        metadata["parent"] = {Parameter.PARENT_GRADIENT.get_name(): parameters[Parameter.PARENT_GRADIENT.get_name()],
                              Parameter.PARENT_COLUMNS_AND_ROWS.get_name():
                                  parameters[Parameter.PARENT_COLUMNS_AND_ROWS.get_name()],
                              Parameter.PARENT_PATH.get_name(): parameters[Parameter.PARENT_PATH.get_name()]}
        self.evolution.start(**parameters)
        self.assertEqual(self.evolution.get_evolution_metadata(), metadata,
                         "incorrect metadata returned from evolution with parent options")
        self.assertEqual(self.evolution.is_fooling_image_generated(), True,
                         "no fooling image generated from evolution with parent options")

    def test_evolution_with_parent_path(self):
        random.seed(1337)
        rel_path_image = "../../tests/testData/images/apples.jpg"
        parameters = {Parameter.PARENT_GRADIENT.get_name(): Parameter.PARENT_GRADIENT.get_default_value(),
                      Parameter.PARENT_COLUMNS_AND_ROWS.get_name():
                          Parameter.PARENT_COLUMNS_AND_ROWS.get_default_value(),
                      Parameter.PARENT_PATH.get_name(): rel_path_image
                      }
        metadata = self.metadata
        metadata["parent"] = parameters
        self.evolution.start(**parameters)
        self.assertEqual(self.evolution.get_evolution_metadata(), metadata,
                         "incorrect metadata returned from evolution with parent path")
        self.assertEqual(self.evolution.is_fooling_image_generated(), True,
                         "no fooling image generated from evolution with parent path")

    def tearDown(self):
        FileManager.MODE = None
        pass


if __name__ == "__main__":
    unittest.main()
