import unittest
import random
from evol.evolution import Evolution
from enums.parameter import Parameter
from enums.mutation import Mutation
from util.fileManager import FileManager


class EvolutionTestCase(unittest.TestCase):
    def setUp(self):
        FileManager.MODE = "test"
        self.config = {"mode": "main",
                       "main": []}
        self.metadata = {Parameter.SUCCESSORS_COUNT.get_name(): Parameter.SUCCESSORS_COUNT.get_default_value(),
                         Parameter.ALLOW_DOWNSWING.get_name(): Parameter.ALLOW_DOWNSWING.get_default_value(),
                         Parameter.MAX_GENERATION_NO.get_name(): Parameter.MAX_GENERATION_NO.get_default_value(),
                         Parameter.CONFIDENCE_THRESHOLD.get_name(): Parameter.CONFIDENCE_THRESHOLD.get_default_value(),
                         "parent": {Parameter.PARENT_GRADIENT.get_name(): Parameter.PARENT_GRADIENT.get_default_value(),
                                    Parameter.PARENT_COLUMNS_AND_ROWS.get_name():
                                        Parameter.PARENT_COLUMNS_AND_ROWS.get_default_value(),
                                    Parameter.PARENT_PATH.get_name(): Parameter.PARENT_PATH.get_default_value()},
                         "mutation": {"mode": Parameter.MUTATION_NAME.get_default_value(),
                                      Parameter.COLUMNS_AND_ROWS.get_name():
                                          Parameter.COLUMNS_AND_ROWS.get_default_value(),
                                      Parameter.GRADIENT.get_name(): Parameter.GRADIENT.get_default_value()}}
        self.evolution = Evolution()

    def test_default_evolution(self):
        random.seed(1337)
        parameters = {}
        self.evolution.start(**parameters)
        self.assertEqual(self.evolution.get_process(), [6.10854, 90.129852, 99.560612],
                         "incorrect process returned from default evolution")
        self.assertEqual(self.evolution.get_generation_no(), 3,
                         "incorrect generation no. returned from default evolution")
        self.assertEqual(self.evolution.get_evolution_metadata(), self.metadata,
                         "incorrect metadata returned from default evolution")
        self.assertEqual(self.evolution.is_fooling_image_generated(), True,
                         "no fooling image generated from default evolution")

    def test_evolution_parameter(self):
        random.seed(1337)
        parameters = {Parameter.SUCCESSORS_COUNT.get_name(): 2,
                      Parameter.ALLOW_DOWNSWING.get_name(): False,
                      Parameter.MAX_GENERATION_NO.get_name(): 3,
                      Parameter.CONFIDENCE_THRESHOLD.get_name(): 90
                      }
        metadata = self.metadata
        metadata[Parameter.SUCCESSORS_COUNT.get_name()] = parameters[Parameter.SUCCESSORS_COUNT.get_name()]
        metadata[Parameter.ALLOW_DOWNSWING.get_name()] = parameters[Parameter.ALLOW_DOWNSWING.get_name()]
        metadata[Parameter.MAX_GENERATION_NO.get_name()] = parameters[Parameter.MAX_GENERATION_NO.get_name()]
        metadata[Parameter.CONFIDENCE_THRESHOLD.get_name()] = parameters[Parameter.CONFIDENCE_THRESHOLD.get_name()]
        self.evolution.start(**parameters)
        self.assertEqual(self.evolution.get_generation_no(), parameters[Parameter.MAX_GENERATION_NO.get_name()],
                         "given max generation no. not applied on evolution with evolution parameter")
        self.assertEqual(self.evolution.get_evolution_metadata(), metadata,
                         "incorrect metadata returned from evolution with evolution parameter")
        self.assertEqual(self.evolution.is_fooling_image_generated(), True,
                         "wrong return value for fooling image generated from evolution with evolution parameter")

    def tearDown(self):
        FileManager.MODE = None
        pass


if __name__ == "__main__":
    unittest.main()
