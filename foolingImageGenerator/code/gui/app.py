"""This module serves as entry point to the graphical user interface of this project.
"""

#!/usr/local/bin/python

import threading
import sys
import tkinter as tk
import numpy as np
import warnings
from tkinter import filedialog
from tkinter import messagebox
from PIL import ImageTk, Image as PIL_Image
from util.image import Image
from util.fileManager import FileManager as fm
from util.figure import Figure
from enums.mutation import Mutation
from enums.parameter import Parameter
from enums.dataType import DataType
from util.validator import Validator
from evol.evolution import Evolution
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg


class App:

    _DEFAULT_NO_GENERATIONS = 20
    _PAD_X = 5
    _PAD_Y = 5
    _WIDTH_ENTRY = 22
    _WIDTH_COLUMN = 320
    _WIDTH_DROPDOWN = 20
    _WIDTH_IMAGE = 340
    _FONT_BOLD = "Verdana 10 bold"
    _FONT = "Verdana 10"
    _PARENT_DEFAULT = _PARENT_UNICOLOR = "unicolor image"
    _PARENT_GRID = "grid image"
    _PARENT_CUSTOM = "user-defined image"
    _PARENT_PIXEL = "pixel image"
    _OPTIONS_PARENT = [_PARENT_UNICOLOR, _PARENT_GRID, _PARENT_PIXEL, _PARENT_CUSTOM]
    _OPTIONS_BOOL = [str(False), str(True)]
    _TXT_MUTATION_OPTIONS = "Mutation Options"
    _TXT_PARENT_OPTIONS = "Parent Options"
    _TXT_MUTATION = Parameter.MUTATION_NAME.get_label_name()
    _TXT_EVOLUTION_OPTIONS = "Evolution Options"
    _TXT_NO_GENERATIONS = Parameter.MAX_GENERATION_NO.get_label_name()
    _TXT_THRESHOLD = Parameter.CONFIDENCE_THRESHOLD.get_label_name()
    _TXT_PARENT_IMG = Parameter.PARENT_PATH.get_label_name()
    _TXT_STOP = "Stop Fooling Image"
    _TXT_DOWNSWING = Parameter.ALLOW_DOWNSWING.get_label_name()
    _TXT_TRAFFIC_SIGN = "Traffic Sign: "
    _TXT_FITNESS = "Fitness: "
    _TXT_FILLED = Parameter.FILLED.get_label_name()
    _TXT_SUCCESSORS_COUNT = Parameter.SUCCESSORS_COUNT.get_label_name()
    _TXT_CIRCLES_ROUND = Parameter.CIRCLES_PER_ROUND.get_label_name()
    _TXT_RECTS_ROUND = Parameter.RECTANGLES_PER_ROUND.get_label_name()
    _TXT_POLYGONS_ROUND = Parameter.POLYGONS_PER_ROUND.get_label_name()
    _TXT_DIAMETER_TO_IMAGE_SIZE = Parameter.DIAMETER_TO_IMAGE_SIZE.get_label_name()
    _TXT_VERTICES = Parameter.VERTICES.get_label_name()
    _TXT_AREAS = Parameter.AREAS.get_label_name()
    _TXT_PERCENT = Parameter.PERCENT.get_label_name()
    _TXT_COLUMNS_AND_ROWS = Parameter.COLUMNS_AND_ROWS.get_label_name()
    _TXT_PARENT_GRADIENT = Parameter.PARENT_GRADIENT.get_label_name()
    _TXT_GRADIENT = Parameter.GRADIENT.get_label_name()
    _DEFAULT_MUTATION = Mutation.POINT_SYMMETRIC_POLYGONS
    _DEFAULT_TRAFFIC_SIGN = "No Traffic Sign"
    _DEFAULT_FITNESS = "No Fitness"
    _DEFAULT_STOP = str(True)
    _DEFAULT_DOWNSWING = str(Parameter.ALLOW_DOWNSWING.get_default_value())
    _DEFAULT_GRADIENT = str(Parameter.GRADIENT.get_default_value())
    _DEFAULT_THRESHOLD = Parameter.CONFIDENCE_THRESHOLD.get_default_value()
    _BTN_CHOOSE = "Choose Image"
    _BTN_RESET = "Reset"
    _BTN_START = "Start"
    _BTN_STOP = "Stop"
    _BTN_SAVE = "Save in high quality"
    _PLT_X_LABEL = "Generation number"
    _PLT_Y_LABEL = "Fitness"
    _PLT_TITLE = "Evolution process"
    _PLT_Y_MIN_MAX = [0, 100]
    _FILE_INITIAL_DIR = "/"
    _FILE_TITLE = "Select file"
    _FILE_PNG_JPEG_NAME = "png and jpeg files"
    _FILE_PNG_DATATYPE = "*.png"
    _FILE_JPG_DATATYPE = "*.jpg"
    _MSG_INFO = "Info"
    _MSG_WARNING = "Warning"
    _MSG_IMG_SAVED = "Image saved"
    _MSG_CONNECTION = "Please check your internet connection."
    _TOOLTIP_SUCCESSORS_COUNT = "Number of successors\ngenerated from parent.\nValue must be greater than 0."
    _TOOLTIP_NO_GENERATION = "Evolution stops after reaching\nthis generation number.\nValue must be greater than 0."
    _TOOLTIP_THRESHOLD = "When confidence exceeds threshold,\ncurrent image is fooling image." \
                         "\nValue must be between 90 and 100."
    _TOOLTIP_PARENT = "Absolute path of image\nused as parent.\nImage must be jpg or png."
    _TOOLTIP_STOP = "Evolution stops early when\nfooling image has been generated."
    _TOOLTIP_DOWNSWING = "Best successor is chosen\nas new parent, even\nif previous parent is fitter."
    _TOOLTIP_PERCENT = "Percentage of pixels\nchanged per generation.\nValue must be between 1 and 100."
    _TOOLTIP_CIRCLES_ROUND = "Number of new circles\nadded in next generation.\nValue must be greater then 0."
    _TOOLTIP_RADIUS_FACTOR = "Largest diameter as\npercentage of the image height.\nValue must be between 1 and 100."
    _TOOLTIP_RECTS_ROUND = "Number of new rectangles\nadded in next generation.\nValue must be greater then 0."
    _TOOLTIP_POLYGONS_ROUND = "Number of new polygons\nadded in next generation.\nValue must be greater then 0."
    _TOOLTIP_VERTICES = "Number of vertices per polygon.\nValue should be greater than 2."
    _TOOLTIP_AREAS = "Number of radial areas into\nwhich image is divided.\nValue must be greater than 0"
    _TOOLTIP_COLUMNS_AND_ROWS = "Number of rows and columns of the grid.\nValue must be greater than 0."
    _PATH_TRAFFIC_SIGNS = "./trafficSignImages/"
    _dropdown_values = {}
    _mutation_map = {}

    def __init__(self, master):
        self.master = master
        self.main_frame = tk.Frame(self.master)
        self.left_top_frame = tk.Frame(self.main_frame)
        self.right_top_frame = tk.Frame(self.main_frame)
        self.left_bottom_frame = tk.Frame(self.main_frame)
        self.right_bottom_frame = tk.Frame(self.main_frame)
        self.__init_gui()
        self.stop_threads = threading.Event()

    def __init_gui(self):
        self.main_frame.pack()
        self.left_top_frame.grid(row=0, column=0)
        self.right_top_frame.grid(row=0, column=1)
        self.left_bottom_frame.grid(row=1, column=0)
        self.right_bottom_frame.grid(row=1, column=1)

        self.__evolution_parameter(self.left_top_frame)
        self.__mutation_parameter(self.right_top_frame)
        self.__diagram(self.left_bottom_frame)
        self.__successor_images(self.right_bottom_frame)

    def __evolution_parameter(self, frame):
        frame.columnconfigure(0, weight=0, minsize=200)
        self.__make_label(frame, self._TXT_EVOLUTION_OPTIONS, 1, bold=True)

        default_successors_count = Parameter.SUCCESSORS_COUNT.get_default_value()
        self.entry_successors_count = self.__make_entry_option(frame, self._TXT_SUCCESSORS_COUNT,
                                                               default_successors_count, 2,
                                                               self._TOOLTIP_SUCCESSORS_COUNT)
        default_no_generations = self._DEFAULT_NO_GENERATIONS
        self.entry_max_generation_no = self.__make_entry_option(frame, self._TXT_NO_GENERATIONS, default_no_generations,
                                                                3, self._TOOLTIP_NO_GENERATION)
        default_threshold = str(self._DEFAULT_THRESHOLD)
        self.entry_threshold = self.__make_entry_option(frame, self._TXT_THRESHOLD, default_threshold, 4,
                                                        self._TOOLTIP_THRESHOLD)
        self.__make_dropdown_option(frame, self._TXT_PARENT_OPTIONS, self._OPTIONS_PARENT, self._PARENT_DEFAULT, 5)
        self.__show_parent_options(self._PARENT_DEFAULT, frame)
        self.__make_dropdown_option(frame, self._TXT_STOP, self._OPTIONS_BOOL, self._DEFAULT_STOP, 8,
                                    self._TOOLTIP_STOP)
        self.__make_dropdown_option(frame, self._TXT_DOWNSWING, self._OPTIONS_BOOL, self._DEFAULT_DOWNSWING, 9,
                                    self._TOOLTIP_DOWNSWING)

    def __mutation_parameter(self, frame):
        frame.columnconfigure(0, weight=0, minsize=200)
        self.__make_label(frame, self._TXT_MUTATION_OPTIONS, 0, column=0, bold=True)

        def get_mutation_options():
            mutations = []
            self._mutation_map = {}
            for mutation in Mutation:
                mutation_name = Figure.beautify_string(mutation.value).title()
                mutations.append(mutation_name)
                self._mutation_map[mutation_name] = mutation
            return mutations
        default_mutation = Figure.beautify_string(self._DEFAULT_MUTATION.value).title()
        self.__make_dropdown_option(frame, self._TXT_MUTATION, get_mutation_options(), default_mutation, 1)
        self.__show_mutation_options(self._DEFAULT_MUTATION, frame)
        self.__make_empty_row(frame, 6)
        self.__make_empty_row(frame, 7)
        self.__make_empty_row(frame, 8)
        self.btn_start = self.__make_button(frame, self._BTN_START, 9)
        self.btn_start.configure(command=self.__start)
        root.bind('<Return>', lambda x: self.__start())
        self.btn_stop = self.__make_button(frame, self._BTN_STOP, 9, column=1, state="disabled")
        self.btn_stop.configure(command=self.__stop_evolution)

    def __diagram(self, frame, evolution=None, max_generation_no=20):
        frame.columnconfigure(0, weight=0, minsize=65)
        self.__make_figure(frame, evolution, max_generation_no)
        self.__make_empty_row(frame, 1)
        self.lbl_traffic_sign = self.__make_label(frame, self._TXT_TRAFFIC_SIGN + self._DEFAULT_TRAFFIC_SIGN, 2, 1)
        self.lbl_fitness = self.__make_label(frame, self._TXT_FITNESS + self._DEFAULT_FITNESS, 3, 1)
        self.traffic_sign_image = self.__make_image(frame, row=2, column=0, rowspan=2, size=Image.SIZE_FINAL)

    def __successor_images(self, frame):
        self.best_successor = self.__make_image(frame, row=0, column=0, resize=True)
        self.btn_save = self.__make_button(frame, self._BTN_SAVE, 1, state="disabled", sticky="N")
        self.btn_save.configure(command=lambda: self.__save(self.image, self.traffic_sign, self.fitness, self.metadata))
        self.__make_scroll_pane(frame)

    def __show_mutation_options(self, mutation, frame):
        mutation_parameters = {Mutation.RANDOM_PIXELS: self.__random_pixels_parameters,
                               Mutation.DRAW_CIRCLES: self.__random_circles_parameters,
                               Mutation.DRAW_RECTANGLES: self.__random_rectangles_parameters,
                               Mutation.POINT_SYMMETRIC_POLYGONS: self.__point_symmetric_polygons_parameters,
                               Mutation.COLOR_GRID: self.__color_grid}
        mutation_parameters[mutation](frame)

    def __random_circles_parameters(self, frame):
        self.__delete_parameters(frame, 2, 4)

        default_circles_round = Parameter.CIRCLES_PER_ROUND.get_default_value()
        self.entry_circles_per_round = self.__make_entry_option(frame, self._TXT_CIRCLES_ROUND, default_circles_round,
                                                                2, self._TOOLTIP_CIRCLES_ROUND)
        default_radius_factor = Parameter.DIAMETER_TO_IMAGE_SIZE.get_default_value()
        self.entry_diameter_to_image_size = self.__make_entry_option(frame, self._TXT_DIAMETER_TO_IMAGE_SIZE,
                                                                     default_radius_factor, 3,
                                                                     self._TOOLTIP_RADIUS_FACTOR)
        default_filled = str(Parameter.FILLED.get_default_value())
        self.__make_dropdown_option(frame, self._TXT_FILLED, self._OPTIONS_BOOL, default_filled, 4)

    def __point_symmetric_polygons_parameters(self, frame):
        self.__delete_parameters(frame, 2, 4)

        default_polygons_round = Parameter.POLYGONS_PER_ROUND.get_default_value()
        self.entry_polygons_round = self.__make_entry_option(frame, self._TXT_POLYGONS_ROUND, default_polygons_round,
                                                             2, self._TOOLTIP_POLYGONS_ROUND)
        default_vertices = Parameter.VERTICES.get_default_value()
        self.entry_vertices = self.__make_entry_option(frame, self._TXT_VERTICES, default_vertices, 3,
                                                       self._TOOLTIP_VERTICES)
        default_areas = Parameter.AREAS.get_default_value()
        self.entry_areas = self.__make_entry_option(frame, self._TXT_AREAS, default_areas, 4, self._TOOLTIP_AREAS)

    def __random_pixels_parameters(self, frame):
        self.__delete_parameters(frame, 2, 4)

        default_percent = Parameter.PERCENT.get_default_value()
        self.entry_percent = self.__make_entry_option(frame, self._TXT_PERCENT, default_percent, 2,
                                                      self._TOOLTIP_PERCENT)
        self.__make_empty_row(frame, 3, 0)
        self.__make_empty_row(frame, 4)

    def __random_rectangles_parameters(self, frame):
        self.__delete_parameters(frame, 2, 4)

        default_rects_round = Parameter.RECTANGLES_PER_ROUND.get_default_value()
        self.entry_rects_round = self.__make_entry_option(frame, self._TXT_RECTS_ROUND, default_rects_round, 2,
                                                          self._TOOLTIP_RECTS_ROUND)
        default_filled = str(Parameter.FILLED.get_default_value())
        self.__make_dropdown_option(frame, self._TXT_FILLED, self._OPTIONS_BOOL, default_filled, 3)
        self.__make_empty_row(frame, 4, 0)

    def __color_grid(self, frame):
        self.__delete_parameters(frame, 2, 4)

        default_rows = Parameter.COLUMNS_AND_ROWS.get_default_value()
        self.entry_colors_rows = self.__make_entry_option(frame, self._TXT_COLUMNS_AND_ROWS, default_rows, 2,
                                                          self._TOOLTIP_COLUMNS_AND_ROWS)
        self.__make_dropdown_option(frame, self._TXT_GRADIENT, self._OPTIONS_BOOL, self._DEFAULT_GRADIENT, 3)
        self.__make_empty_row(frame, 4, 0)

    def __show_parent_options(self, parent_option, frame):
        parent_parameters = {self._PARENT_UNICOLOR: self.__unicolor_parent,
                             self._PARENT_GRID: self.__grid_parent,
                             self._PARENT_CUSTOM: self.__custom_parent,
                             self._PARENT_PIXEL: self.__pixel_parent}
        parent_parameters[parent_option](frame)

    def __unicolor_parent(self, frame):
        self.__delete_parameters(frame, 6, 7)
        self.__make_dropdown_option(frame, self._TXT_PARENT_GRADIENT, self._OPTIONS_BOOL, self._DEFAULT_GRADIENT, 6)
        self.__make_empty_row(frame, 7, 1)

    def __grid_parent(self, frame):
        self.__delete_parameters(frame, 6, 7)
        self.entry_parent_rows_columns = self.__make_entry_option(frame, self._TXT_COLUMNS_AND_ROWS,
                                                                  Parameter.COLUMNS_AND_ROWS.get_default_value(),
                                                                  6, self._TOOLTIP_COLUMNS_AND_ROWS)
        self.__make_dropdown_option(frame, self._TXT_PARENT_GRADIENT, self._OPTIONS_BOOL,
                                    str(Parameter.PARENT_GRADIENT.get_default_value()), 7)

    def __custom_parent(self, frame):
        self.__delete_parameters(frame, 6, 7)
        self.entry_parent = self.__make_entry_option(frame, self._TXT_PARENT_IMG, "", 6,
                                                     self._TOOLTIP_PARENT)
        btn_choose_parent = self.__make_button(frame, self._BTN_CHOOSE, 7, column=1, sticky="W")
        btn_choose_parent.configure(command=lambda: self.__make_file_picker(frame))
        btn_reset_parent = self.__make_button(frame, self._BTN_RESET, 7, column=1, sticky="E")
        btn_reset_parent.configure(command=lambda: self.__reset_parent_image_path(frame))

    def __pixel_parent(self, frame):
        self.__delete_parameters(frame, 6, 7)
        self.__make_empty_row(frame, 6, 0)
        self.__make_empty_row(frame, 7)

    def __start(self):
        evolution = Evolution()
        is_valid, (mutation, successors_count, max_generation_no, stop_image, allow_downswing,
                   confidence_threshold, parameters) = self.__validate_parameters(self.__get_parameter())
        if not is_valid:
            return
        mutation, successors_count, max_generation_no, allow_downswing, high_quality, self.confidence_threshold,\
        parameters = evolution.prepare_evolution(mutation, successors_count, max_generation_no, allow_downswing,
                                                 False, confidence_threshold, False, **parameters)
        self.__initialize_gui_for_evolution(self.confidence_threshold, successors_count, max_generation_no)
        self.stop_threads = threading.Event()
        thread = threading.Thread(target=self.__evolve,
                                  args=(evolution, self.confidence_threshold, max_generation_no, stop_image,
                                        successors_count, mutation, allow_downswing, parameters))
        thread.Daemon = True
        thread.start()
        root.after(3000, lambda: self.__check_connection_problems(evolution))

    def __check_connection_problems(self, evolution):
        if not evolution.get_generation_no() > 0:
            messagebox.showwarning(self._MSG_WARNING, self._MSG_CONNECTION)
            self.__stop_evolution()

    def __validate_parameters(self, evolution_parameters):
        is_valid = True
        mutation, successors_count, max_generation_no, stop_image, allow_downswing, confidence_threshold, \
            parameters = evolution_parameters
        with warnings.catch_warnings(record=True) as w:
            successors_count, max_generation_no, allow_downswing, confidence_threshold, mutation, parameters = \
                Validator.validate_evolution_with_strings(successors_count, max_generation_no, allow_downswing,
                                                          confidence_threshold, mutation, parameters)
            if len(w) >= 1:
                is_valid = False
                warning_text = ""
                for warning in w:
                    warning_text += str(warning.message) + "\n"
                messagebox.showwarning(self._MSG_WARNING, warning_text)
                pass
            return is_valid, (mutation, successors_count, max_generation_no, stop_image, allow_downswing,
                              confidence_threshold, parameters)

    def __get_parameter(self):
        successors_count = self.entry_successors_count.get()
        max_generation_no = self.entry_max_generation_no.get()
        parent_parameters = self.__get_parent_options(self._dropdown_values[self._TXT_PARENT_OPTIONS])
        allow_downswing = self._dropdown_values[self._TXT_DOWNSWING]
        stop_image = Validator.convert_str_to_bool(self._dropdown_values[self._TXT_STOP])
        mutation_name = self._dropdown_values[self._TXT_MUTATION]
        mutation = self._mutation_map[mutation_name]
        mutation_parameters = self.__get_mutation_options(mutation)
        parameters = parent_parameters
        parameters.update(mutation_parameters)
        self.confidence_threshold = self.entry_threshold.get()
        return (mutation, successors_count, max_generation_no, stop_image, allow_downswing,
                self.confidence_threshold, parameters)

    def __get_mutation_options(self, mutation):
        mutation_parameters = {Mutation.RANDOM_PIXELS:
                                   lambda x: {Parameter.PERCENT.get_name(): self.entry_percent.get()},
                               Mutation.DRAW_CIRCLES:
                                   lambda x: {Parameter.CIRCLES_PER_ROUND.get_name():
                                                  self.entry_circles_per_round.get(),
                                              Parameter.DIAMETER_TO_IMAGE_SIZE.get_name():
                                                  self.entry_diameter_to_image_size.get(),
                                              Parameter.FILLED.get_name():
                                                  self._dropdown_values[self._TXT_FILLED]},
                               Mutation.DRAW_RECTANGLES:
                                   lambda x: {Parameter.RECTANGLES_PER_ROUND.get_name():
                                                  self.entry_rects_round.get(),
                                              Parameter.FILLED.get_name():
                                                  self._dropdown_values[self._TXT_FILLED]},
                               Mutation.POINT_SYMMETRIC_POLYGONS:
                                   lambda x: {Parameter.VERTICES.get_name(): self.entry_vertices.get(),
                                              Parameter.AREAS.get_name(): self.entry_areas.get(),
                                              Parameter.POLYGONS_PER_ROUND.get_name():
                                                  self.entry_polygons_round.get()},
                               Mutation.COLOR_GRID:
                                   lambda x: {Parameter.COLUMNS_AND_ROWS.get_name(): self.entry_colors_rows.get(),
                                              Parameter.GRADIENT.get_name(): self._dropdown_values[self._TXT_GRADIENT]}}
        return mutation_parameters[mutation](None)

    def __get_parent_options(self, parent_option):
        parent_parameters = {self._PARENT_UNICOLOR:
                                 lambda x: {Parameter.PARENT_PATH.get_name(): Parameter.PATH.get_default_value(),
                                            Parameter.PARENT_GRADIENT.get_name():
                                                self._dropdown_values[self._TXT_PARENT_GRADIENT],
                                            Parameter.PARENT_COLUMNS_AND_ROWS.get_name():
                                                Parameter.PARENT_COLUMNS_AND_ROWS.get_default_value()},
                             self._PARENT_GRID:
                                 lambda x: {Parameter.PARENT_PATH.get_name(): Parameter.PATH.get_default_value(),
                                            Parameter.PARENT_GRADIENT.get_name():
                                                self._dropdown_values[self._TXT_PARENT_GRADIENT],
                                            Parameter.PARENT_COLUMNS_AND_ROWS.get_name():
                                                self.entry_parent_rows_columns.get()},
                             self._PARENT_CUSTOM:
                                 lambda x: {Parameter.PARENT_PATH.get_name(): self.entry_parent.get(),
                                            Parameter.PARENT_GRADIENT.get_name():
                                                Parameter.PARENT_GRADIENT.get_default_value(),
                                            Parameter.PARENT_COLUMNS_AND_ROWS.get_name():
                                                Parameter.PARENT_COLUMNS_AND_ROWS.get_default_value()},
                             self._PARENT_PIXEL:
                                 lambda x: {Parameter.PARENT_PATH.get_name(): Parameter.PATH.get_default_value(),
                                            Parameter.PARENT_GRADIENT.get_name():
                                                False,
                                            Parameter.PARENT_COLUMNS_AND_ROWS.get_name(): Image.SIZE_FINAL}
                             }
        return parent_parameters[parent_option](None)

    def __evolve(self, evolution, threshold, max_generation_no, stop_image, successors_count, mutation,
                 allow_downswing, parameters):
        while not self.stop_threads.is_set() and evolution.get_generation_no() <= max_generation_no:
            if not stop_image or not evolution.is_fooling_image_generated():
                try:
                    with warnings.catch_warnings(record=True) as w:
                        evolution.evolve(successors_count, mutation, allow_downswing, False, False, **parameters)
                        if len(w) >= 1:
                            warning_text = ""
                            for warning in w:
                                warning_text += str(warning.message) + "\n"
                            messagebox.showwarning(self._MSG_WARNING, warning_text)
                            pass
                    if not self.stop_threads.is_set():
                        self.__update_gui(threshold, evolution, max_generation_no, successors_count)
                except RuntimeError:
                    sys.exit(1)
            else:
                break
        self.__stop_evolution()
        self.__activate_start_button()

    def __stop_evolution(self):
        try:
            self.btn_stop.config(state="disabled")
            self.stop_threads.set()
        except RuntimeError:
            sys.exit(1)

    def __activate_start_button(self):
        self.btn_start.config(state="normal")

    def __save(self, image, traffic_sign, fitness, metadata):
        image_name = fm.build_name(fm.build_name(traffic_sign, str(fitness)))
        fm.save_in_high_quality(DataType.HIGH_QUALITY_IMAGE, image, image_name, traffic_sign, metadata)
        messagebox.showinfo(self._MSG_INFO, self._MSG_IMG_SAVED)

    def __initialize_gui_for_evolution(self, confidence_threshold, successors_count, max_generation_no):
        self.btn_start.config(state="disabled")
        self.btn_stop.config(state="normal")
        self.btn_save.config(state="disabled")
        self.scroll_frame.destroy()
        self.__make_scroll_pane(self.right_bottom_frame)
        self.__make_figure(self.left_bottom_frame, None, max_generation_no)
        self.__update_gui(confidence_threshold, successors_count=successors_count)

    def __update_gui(self, threshold, evolution=None, max_generation_no=20,
                     successors_count=Parameter.SUCCESSORS_COUNT.get_default_value()):
        self.__make_figure(self.left_bottom_frame, evolution, max_generation_no, threshold)
        self.__update_classification_information(evolution)
        self.__update_successor_area(threshold, evolution, successors_count)

    def __update_classification_information(self, evolution):
        txt_traffic_sign = self._TXT_TRAFFIC_SIGN + self._DEFAULT_TRAFFIC_SIGN
        txt_fitness = self._TXT_FITNESS + self._DEFAULT_FITNESS
        file_name = None
        if evolution is not None:
            self.traffic_sign = evolution.get_parent_traffic_sign()
            self.fitness = evolution.get_parent_fitness()
            txt_traffic_sign = self._TXT_TRAFFIC_SIGN + Figure.shorten_traffic_signs([self.traffic_sign])[0]
            txt_fitness = self._TXT_FITNESS + str(self.fitness)
            file_name = fm.build_filename(self.traffic_sign, DataType.IMAGE.get_first_file_extension())
        self.__update_image(self.traffic_sign_image, file_name=file_name, size=Image.SIZE_FINAL)
        self.lbl_traffic_sign.configure(text=txt_traffic_sign)
        self.lbl_fitness.configure(text=txt_fitness)

    def __update_successor_area(self, threshold, evolution=None,
                                successors_count=Parameter.SUCCESSORS_COUNT.get_default_value()):
        best_successor_array = self.__get_default_image()
        self.btn_save.config(state="disabled")
        if evolution is not None:
            self.image = evolution.get_parent_fooling_image()
            self.metadata = fm.build_metadata_for_file(evolution.get_evolution_metadata(), self.image.image_metadata)
            best_successor_array = self.image.img
            if self.image.is_fooling_image(threshold):
                self.btn_save.config(state="normal")
        self.__update_image(self.best_successor, best_successor_array, size=self._WIDTH_IMAGE)
        self.__update_successor_images(evolution, successors_count)

    def __update_image(self, image_object, new_image_array=None, file_name=None, size=Image.SIZE):
        default_image = self.__get_default_image(size)
        image_array = Image.convert_image_array_for_presentation(default_image)
        image = PIL_Image.fromarray(image_array, "RGB")
        if file_name is not None:
            rel_path = fm.get_relative_path_for_data_type(DataType.TRAFFIC_SIGN_IMAGE, [file_name])
            try:
                image = PIL_Image.open(rel_path)
            except FileNotFoundError:
                pass
        elif new_image_array is not None:
            converted_array = Image.convert_image_array_for_presentation(new_image_array)
            image = PIL_Image.fromarray(converted_array, "RGB")
        image_array = image.resize((size, size))
        render = ImageTk.PhotoImage(image_array)
        image_object.configure(image=render)
        image_object.image = render

    def __update_successor_images(self, evolution, successors_count):
        self.data_frame.grid_forget()
        self.__add_data(self.data_frame, evolution, successors_count)

    def __make_label(self, frame, label_text, row, column=0, bold=False):
        font = self._FONT
        if bold:
            font = self._FONT_BOLD
        label = tk.Label(frame, text=label_text, font=font)
        label.grid(row=row, column=column, padx=self._PAD_X, pady=self._PAD_Y, sticky="W")
        return label

    def __make_button(self, frame, text, row, column=0, state="normal", sticky="EW"):
        button = tk.Button(frame, text=text, font=self._FONT, state=state)
        button.grid(row=row, column=column, padx=self._PAD_X, pady=self._PAD_Y-4, sticky=sticky)
        return button

    def __make_image(self, frame, path=None, row=0, column=0, rowspan=1, columnspan=1, size=Image.SIZE, resize=False):
        default_image = self.__get_default_image(size)
        image_array = Image.convert_image_array_for_presentation(default_image)
        image = PIL_Image.fromarray(image_array, "RGB")
        if path is not None:
            try:
                image = PIL_Image.open(path)
            except FileNotFoundError:
                pass
        if resize:
            image = image.resize((self._WIDTH_IMAGE, self._WIDTH_IMAGE), PIL_Image.ANTIALIAS)
        render = ImageTk.PhotoImage(image)
        img = tk.Label(frame, image=render)
        img.image = render
        img.grid(row=row, column=column, rowspan=rowspan, columnspan=columnspan, sticky="W")
        return img

    def __make_scroll_pane(self, frame):
        height = Image.SIZE_FINAL + self._PAD_X
        self.scroll_frame = tk.Frame(frame, relief="groove", width=self._WIDTH_IMAGE, height=height, bd=1)
        self.scroll_frame.grid(row=2, column=0)

        figure = tk.Canvas(self.scroll_frame)
        self.data_frame = tk.Frame(figure)
        scrollbar = tk.Scrollbar(self.scroll_frame, orient="horizontal", command=figure.xview)
        figure.configure(xscrollcommand=scrollbar.set)

        def configure(event):
            figure.configure(scrollregion=figure.bbox("all"), width=self._WIDTH_IMAGE, height=height)

        scrollbar.pack(side="bottom", fill="x")
        figure.pack(side="left")
        figure.create_window((0, 0), window=self.data_frame, anchor='nw')
        frame.bind("<Configure>", configure)
        self.__add_data(self.data_frame)

    def __make_empty_row(self, frame, row, space=1):
        empty_row = tk.Label(frame, text="           ")
        empty_row.grid(row=row, column=0, padx=self._PAD_X, pady=self._PAD_Y + space)

    def __make_dropdown_option(self, frame, label, options, default_option, row, tooltip_text=None):
        self.__make_label(frame, label + ": ", row)
        self._dropdown_values[label] = default_option
        default_option_var = tk.StringVar(root)
        default_option_var.set(default_option)
        dropdown = tk.OptionMenu(frame, default_option_var, *options,
                                 command=lambda x: self.__save_dropdown_value(default_option_var, label, frame))
        dropdown.config(width=self._WIDTH_DROPDOWN, font=self._FONT)
        dropdown.grid(row=row, column=1, padx=self._PAD_X, pady=self._PAD_Y-5, sticky="W")
        return dropdown

    def __make_figure(self, frame, evolution=None, max_generation_no=20, threshold=_DEFAULT_THRESHOLD):
        fig = self.__get_plot(evolution, max_generation_no, threshold)
        old_size = fig.get_size_inches()
        factor = 0.8
        fig.set_size_inches([factor * s for s in old_size])
        if hasattr(self, 'canvas'):
            self.figure.get_tk_widget().destroy()
        self.figure = FigureCanvasTkAgg(fig, master=frame)
        self.figure.get_tk_widget().grid(row=0, column=0, columnspan=2, sticky="NW")

    def __make_entry_option(self, frame, label_text, default_entry, row, tooltip_text):
        beautified_text = label_text + ": "
        self.__make_label(frame, beautified_text, row)
        default_entry_var = tk.StringVar(frame, default_entry)
        entry = tk.Entry(frame, textvariable=default_entry_var, width=self._WIDTH_ENTRY, font=self._FONT)
        entry.grid(row=row, column=1, padx=self._PAD_X, pady=self._PAD_Y, sticky="W")
        return entry

    def __make_file_picker(self, frame):
        file_path = filedialog.askopenfilename(initialdir=self._FILE_INITIAL_DIR, title=self._FILE_TITLE,
                                               filetypes=[(self._FILE_PNG_JPEG_NAME,
                                                           self._FILE_PNG_DATATYPE + " " + self._FILE_JPG_DATATYPE)])
        var_file_path = tk.StringVar(frame, file_path)
        self.entry_parent.config(text=var_file_path)

    def __add_data(self, frame, evolution=None, successors_count=Parameter.SUCCESSORS_COUNT.get_default_value()):
        for i in range(successors_count):
            image_path = None
            if evolution is not None and evolution.get_generation_no() > 1:
                image_name = fm.build_name(fm.IMAGE_PREFIX, str(evolution.get_generation_no() - 1), str(i))
                filename = fm.build_filename(image_name, DataType.IMAGE.get_first_file_extension())
                image_path = fm.build_relative_path([fm.get_relative_path_for_data_type(DataType.IMAGE), filename])
            self.__make_image(frame, path=image_path, row=0, column=i, size=Image.SIZE_FINAL)

    def __get_plot(self, evolution, max_generation_no, threshold=_DEFAULT_THRESHOLD):
        coordinates = [0], [0]
        if evolution is not None:
            coordinates = list(range(0, evolution.get_generation_no())), evolution.get_process()
        title = self._PLT_TITLE
        y_label = self._PLT_Y_LABEL
        x_label = self._PLT_X_LABEL
        y_min_max = self._PLT_Y_MIN_MAX
        x_min_max = [0, max_generation_no]
        fig = Figure.draw_plots([coordinates], title, x_label, y_label, x_min_max=x_min_max,
                                y_min_max=y_min_max, show_dots=True, smooth=False, threshold=threshold)
        return fig

    @staticmethod
    def __delete_parameters(frame, start_row, end_row):
        children = []
        for child in frame.children.values():
            try:
                info = child.grid_info()
                if start_row <= int(info['row']) <= end_row:
                    children.append(child)
            except AttributeError:
                pass
        for child in children:
            child.destroy()

    def __save_dropdown_value(self, var, label, frame):
        value = var.get()
        self._dropdown_values[label] = value
        if label == self._TXT_MUTATION:
            self.__show_mutation_options(self._mutation_map[value], frame)
        if label == self._TXT_PARENT_OPTIONS:
            self.__show_parent_options(value, frame)

    def __reset_parent_image_path(self, frame):
        parent_default = tk.StringVar(frame, "")
        self.entry_parent.config(text=parent_default)

    @staticmethod
    def __get_default_image(size=Image.SIZE):
        return np.full([size, size, 3], 1.0)


if __name__ == '__main__':
    root = tk.Tk()
    root.title("Fooling Image Generator")
    app = App(root)
    root.mainloop()
