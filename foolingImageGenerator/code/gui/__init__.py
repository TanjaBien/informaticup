"""This package includes all classes for the graphical user interface.

Submodules
==========

.. autosummary::

    gui
"""
