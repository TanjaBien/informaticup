"""This module can be used to build warning messages and print them to the console.
"""

import warnings
from enums.parameter import Parameter


class CustomWarnings:

    @staticmethod
    def print_warning(message):
        def custom_formatwarning(msg, *args, **kwargs):
            return str(msg) + '\n'
        warnings.formatwarning = custom_formatwarning
        warnings.warn(message)

    @staticmethod
    def build_message_invalid_mutation(mutation, test_abortion=False, set_to_default=False):
        message = f"Mutation '{str(mutation)}' does not exist.'"
        if test_abortion:
            message += " Analysis was aborted."
        if set_to_default:
            default_mutation = Parameter.MUTATION_NAME.get_default_value()
            message += f" The default value '{default_mutation.name} ( = {default_mutation.value})' " \
                f"will be used for execution."
        return message

    @staticmethod
    def build_message_invalid_parameter_value(parameter_name, input_value, default_value, gui_message):
        additional_info = ""
        if gui_message:
            parameter_name = Parameter(parameter_name).get_label_name()
        else:
            additional_info = f" Its value was reset to default ({str(default_value)})."
        message = f"For parameter '{parameter_name}' an invalid input ({str(input_value)}) was defined." \
                  f"{additional_info}"
        return message

    @staticmethod
    def build_message_invalid_mutation_parameter_name(parameter_name, mutation):
        message = f"Parameter '{parameter_name}' is not applicable for mutation '{mutation.value}'. " \
            f"Therefore the parameter was deleted from the parameter list and won't be considered in the execution."
        return message

    @staticmethod
    def build_message_invalid_parameter_test_params(parameter_name, **invalid_params):
        invalid_values = ""
        for key in invalid_params:
            invalid_values += key + " = " + str(invalid_params[key]) + ", "
        message = f"The values ({invalid_values[:-2]}) cannot be used to test '{parameter_name}'. Analysis was aborted."
        return message

    @staticmethod
    def build_message_invalid_parameter_test_name(parameter_name, mutation):
        message = f"Parameter '{parameter_name}' is not applicable for mutation '{mutation.value}'." \
            f" Analysis was aborted."
        return message
