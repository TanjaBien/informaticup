"""This module can be used to access files.
"""

import os
import sys
import shutil
import cv2
import numpy as np
import json
import random
from enums.dataType import DataType
from util.customWarnings import CustomWarnings
import util.image as image_module


class FileManager:
    IMAGE_PREFIX = "generated_image"
    METADATA_EVOLUTION = "evolution_metadata"
    METADATA_IMAGE = "image_metadata"
    _PATH_TESTS = "tests"
    _PATH_TEST_DATA = "testData"
    _PATH_TEST_IMAGES = "testImages"
    _PATH_PARENT_DIR = os.pardir
    _PATH_EVOLUTION_CODE = "evol"
    _PATH_GUI_CODE = "gui"
    _PATH_FOOLING_IMAGES = "foolingImages"
    _DEFAULT_WORKING_DIR = os.path.abspath(os.path.dirname(__file__))
    os.chdir(_DEFAULT_WORKING_DIR)
    MODE = None

    @staticmethod
    def is_file_existent(dir_path):
        return os.path.exists(dir_path)

    @staticmethod
    def is_image(dir_path):
        split_dir_path = dir_path.split(".")
        split_dir_path.remove(split_dir_path[0])
        file_extension = split_dir_path[-1]
        return file_extension in DataType.SAMPLE_IMAGE.get_all_file_extensions()

    @staticmethod
    def __create_folder(dir_path):
        try:
            if not os.path.exists(dir_path):
                os.makedirs(dir_path)
        except OSError:
            print(f"Error: Creating directory. {dir_path}")

    @staticmethod
    def __delete_folder(dir_path):
        try:
            if os.path.exists(dir_path):
                shutil.rmtree(dir_path)
        except OSError:
            print(f"Error: Deleting directory. {dir_path}")

    @staticmethod
    def __delete_file(file_path):
        try:
            if os.path.exists(file_path):
                os.remove(file_path)
        except OSError:
            print(f"Error: Deleting file. {file_path}")

    @staticmethod
    def open_image_file(image_name, options="rb", custom_folder=False):
        filename = FileManager.build_filename(image_name, DataType.IMAGE.get_first_file_extension())
        if not custom_folder:
            relative_path = FileManager.get_relative_path_for_data_type(DataType.IMAGE, [filename])
        else:
            relative_path = filename
        my_path = os.path.abspath(os.path.dirname(__file__))
        path = os.path.join(my_path, relative_path)
        if os.path.exists(path):
            return open(path, options)
        else:
            return None

    @staticmethod
    def load_generation_images(generation, successors_count, size=None):
        generation_images = []
        for i in range(0, successors_count):
            image_name = FileManager.build_name(FileManager.IMAGE_PREFIX, str(generation), str(i))
            image = FileManager.open_image_file(image_name)
            if size is not None:
                image = image_module.Image.resize(image, size)
            generation_images.append(image)
        return generation_images

    @staticmethod
    def build_relative_path(directories):
        path = ""
        for i in range(len(directories)):
            path = os.path.join(path, str(directories[i]))
        return path

    @staticmethod
    def build_filename(name, data_type):
        return name + "." + data_type

    @staticmethod
    def show_image(image_name, image_array):
        filename = FileManager.build_filename(image_name, DataType.IMAGE.get_first_file_extension())
        cv2.imshow(filename, image_array)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    @staticmethod
    def get_image_array_from_path(filename, width, height):
        if not FileManager.is_file_existent(filename):
            CustomWarnings.print_warning(f"Image '{filename}' not found")
            return image_module.Image.get_initial_image_array(width, height)
        stream = open(filename, "rb")
        image_bytes = bytearray(stream.read())
        image_array = np.asarray(image_bytes, dtype=np.uint8)
        image = cv2.imdecode(image_array, cv2.IMREAD_COLOR)
        if image is not None:
            image = image / image_module.Image.RGB_CONVERT
            image = image_module.Image.resize(image, width, height)
            image = image_module.Image.satisfy_float_value_range(image)
        else:
            CustomWarnings.print_warning(f"Given path '{filename}' is not an valid path for an image.")
            image = image_module.Image.get_initial_image_array(width, height)
        return image

    @staticmethod
    def open_json_file(file_name):
        data = {}
        with open(file_name) as json_data:
            data = json.load(json_data)
        return data

    @staticmethod
    def save(data_type, file, name, **parameters):
        data_types = {DataType.IMAGE: FileManager.__save_image,
                      DataType.FOOLING_IMAGE: FileManager.__save_fooling_image,
                      DataType.PLOT: FileManager.__save_plot}
        if FileManager.MODE == "test" and not DataType.IMAGE:
            return
        try:
            return data_types[data_type](data_type, file, name, **parameters)
        except TypeError as error:
            raise Exception(error)

    @staticmethod
    def __save_metadata(rel_path, name, metadata):
        data = json.dumps(metadata)
        metadata_json = json.loads(data)
        filename = FileManager.build_filename(name, DataType.METADATA.get_first_file_extension())
        file_path = FileManager.build_relative_path([rel_path, filename])
        with open(file_path, "w") as file:
            json.dump(metadata_json, file)

    @staticmethod
    def build_metadata_for_file(evolution_metadata, image_metadata):
        metadata = {FileManager.METADATA_EVOLUTION: evolution_metadata,
                    FileManager.METADATA_IMAGE: image_metadata}
        return metadata

    @staticmethod
    def __get_unique_name(data_type, name, rel_path_folder=None):
        if FileManager.__is_filename_used(data_type, name, rel_path_folder):
            name_components = name.split("_")
            name_components[-1] = FileManager.__generate_id()
            name = name_components[0]
            for i in range(len(name_components) - 1):
                name = FileManager.build_name(name, name_components[i + 1])
            name = FileManager.__get_unique_name(data_type, name, rel_path_folder)
        return name

    @staticmethod
    def __is_filename_used(data_type, name, rel_path_folder=None):
        filename = FileManager.build_filename(name, data_type.get_first_file_extension())
        rel_path_file = FileManager.build_relative_path([rel_path_folder, filename])
        return os.path.exists(rel_path_file)

    @staticmethod
    def __generate_id():
        characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
        random_id = ''
        for i in range(0, 6):
            random_id += random.choice(characters)
        return random_id

    @staticmethod
    def build_name(prefix, steam=None, suffix=None):
        if suffix is not None:
            return str(prefix) + "_" + str(steam) + "_" + str(suffix)
        if steam is not None:
            return str(prefix) + "_" + str(steam)
        return str(prefix) + "_" + FileManager.__generate_id()

    @staticmethod
    def __save_image(data_type, image, image_name):
        rel_path_folder = FileManager.get_relative_path_for_data_type(data_type)
        if image_name == FileManager.build_name(FileManager.IMAGE_PREFIX, str(0), str(0)):
            FileManager.__delete_folder(rel_path_folder)
        rel_path_image = FileManager.__get_relative_path_for_file(data_type, rel_path_folder, image_name)
        converted_img = image_module.Image.convert_image_array_to_int_value_range(image.img)
        resized_img = image_module.Image.resize(converted_img, image_module.Image.SIZE_FINAL)
        cv2.imwrite(rel_path_image, resized_img)

    @staticmethod
    def __save_png_image_with_umlaut(image, rel_path_image):
        file = open(rel_path_image, "wb")
        image_encoded = cv2.imencode("." + DataType.IMAGE.get_first_file_extension(), image)[1]
        image_bytes = image_encoded.flatten()
        file.write(image_bytes)
        file.close()

    @staticmethod
    def __save_fooling_image(data_type, image, image_name, folder_name, metadata, high_quality):
        rel_path_folder = FileManager.get_relative_path_for_data_type(data_type, [folder_name])
        image_name = FileManager.__get_unique_name(data_type, image_name, rel_path_folder)
        rel_path_image = FileManager.__get_relative_path_for_file(data_type, rel_path_folder, image_name)
        converted_img = image_module.Image.convert_image_array_to_int_value_range(image.img)
        resized_img = image_module.Image.resize(converted_img, image_module.Image.SIZE_FINAL)
        FileManager.__save_png_image_with_umlaut(resized_img, rel_path_image)
        if high_quality:
            FileManager.save_in_high_quality(DataType.HIGH_QUALITY_IMAGE, image, image_name, folder_name, metadata)
        FileManager.__save_metadata(rel_path_folder, image_name, metadata)

    @staticmethod
    def save_in_high_quality(data_type, image, image_name, folder_name, metadata):
        rel_path_folder = FileManager.get_relative_path_for_data_type(data_type, [folder_name])
        image_name = FileManager.__get_unique_name(data_type, image_name, rel_path_folder)
        rel_path_image = FileManager.__get_relative_path_for_file(data_type, rel_path_folder, image_name)
        converted_image = image_module.Image.convert_image_array_to_int_value_range(image.img)
        FileManager.__save_png_image_with_umlaut(converted_image, rel_path_image)
        FileManager.__save_metadata(rel_path_folder, image_name, metadata)

    @staticmethod
    def __save_plot(data_type, plot, plot_name, data, metadata):
        rel_path_folder = FileManager.get_relative_path_for_data_type(data_type)
        plot_name = FileManager.__get_unique_name(data_type, plot_name, rel_path_folder)
        rel_path_plot = FileManager.__get_relative_path_for_file(data_type, rel_path_folder, plot_name)
        plot.savefig(rel_path_plot, bbox_inches="tight")
        print(metadata)
        FileManager.__save_plot_data(data, rel_path_folder, plot_name)
        FileManager.__save_metadata(rel_path_folder, plot_name, metadata)

    @staticmethod
    def __save_plot_data(data, rel_path_plot, plot_name):
        filename = FileManager.build_filename(plot_name, DataType.PLOT_DATA.get_first_file_extension())
        rel_path_file = FileManager.build_relative_path([rel_path_plot, filename])
        np.save(rel_path_file, data)

    @staticmethod
    def get_relative_path_for_data_type(data_type, subfolders=None):
        root_directories = {DataType.PLOT: [FileManager._PATH_PARENT_DIR, FileManager._PATH_PARENT_DIR],
                            DataType.SAMPLE_IMAGE: [FileManager._PATH_PARENT_DIR, FileManager._PATH_PARENT_DIR],
                            DataType.CONFIG_FILE: [FileManager._PATH_PARENT_DIR, FileManager._PATH_PARENT_DIR],
                            DataType.IMAGE: [FileManager._PATH_PARENT_DIR, FileManager._PATH_EVOLUTION_CODE],
                            DataType.TRAFFIC_SIGN_IMAGE: [FileManager._PATH_PARENT_DIR,
                                                          FileManager._PATH_GUI_CODE],
                            DataType.HIGH_QUALITY_IMAGE: [FileManager._PATH_PARENT_DIR,
                                                          FileManager._PATH_PARENT_DIR,
                                                          FileManager._PATH_FOOLING_IMAGES],
                            DataType.FOOLING_IMAGE: [FileManager._PATH_PARENT_DIR,
                                                     FileManager._PATH_PARENT_DIR,
                                                     FileManager._PATH_FOOLING_IMAGES],
                            DataType.TEST_DATA: [FileManager._PATH_PARENT_DIR,
                                                 FileManager._PATH_TESTS]}
        directories = root_directories[data_type]
        if hasattr(sys, '_MEIPASS'):
            del directories[0]
            del directories[0]
            if data_type in [DataType.HIGH_QUALITY_IMAGE, DataType.FOOLING_IMAGE, DataType.CONFIG_FILE]:
                directories.insert(0, os.path.dirname(sys.executable))
            else:
                directories.insert(0, sys._MEIPASS)
        directories.append(data_type.get_folder_name())
        if subfolders:
            directories.extend(subfolders)
        rel_path = FileManager.build_relative_path(directories)
        return rel_path

    @staticmethod
    def __get_relative_path_for_file(data_type, rel_path_folder, name):
        FileManager.__create_folder(rel_path_folder)
        filename = FileManager.build_filename(name, data_type.get_first_file_extension())
        rel_path_file = FileManager.build_relative_path([rel_path_folder, filename])
        return rel_path_file
