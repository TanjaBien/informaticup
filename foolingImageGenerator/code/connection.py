""" This module can be used to send requests to the given API for InformatiCup 2019.
"""

import requests
import time
import sys
from enums.dataType import DataType
from util.fileManager import FileManager as fm
from util.customWarnings import CustomWarnings


class Connection:
    CONFIG_FILE = "connection_config.json"

    def __init__(self):
        self.response = []
        rel_path_folder = fm.get_relative_path_for_data_type(DataType.CONFIG_FILE)
        rel_path_config_file = fm.build_relative_path([rel_path_folder, Connection.CONFIG_FILE])
        config = fm.open_json_file(rel_path_config_file)
        self.url = config["url"]
        self.key = config["key"]
        self.data = {"key": self.key}
        self._DELAY = 1.0

    def post(self, image_name, custom_folder=False): 
        time.sleep(self._DELAY)
        image = fm.open_image_file(image_name, custom_folder=custom_folder)
        if image is not None:
            try:
                self.response = requests.post(url=self.url, files={"image": image}, data=self.data)
                image.close()
                status = self.get_status_code()
                if status == 200:
                    pass
                elif status == 429:
                    CustomWarnings.print_warning("Too Many requests. Program waits 60 seconds.")
                    time.sleep(60)
                    self.post(image_name, custom_folder)
                else:
                    raise Exception(status, self.response.content)
            except requests.exceptions.RequestException as e:
                print(e)
                sys.exit(1)
            except Exception as ex:
                CustomWarnings.print_warning(f"Exception in post request {str(ex)} was ignored")
                # sys.exit(1)
        else:
            raise FileNotFoundError(f"Image {image_name}.png not found.")

    def get_status_code(self):
        return self.response.status_code

    def __get_best_match(self):
        return self.response.json()[0]

    def get_highest_confidence(self):
        return self.__get_best_match()["confidence"]

    def get_highest_traffic_sign(self):
        return self.__get_best_match()["class"]
