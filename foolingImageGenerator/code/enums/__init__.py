"""This package includes all enums used troughout the project.

Submodules
==========

.. autosummary::

    dataType
    mutation
    parameter
"""
