"""This module includes the enum for data types which are used throughout the project.
"""

from enum import Enum


class DataType(Enum):
    IMAGE = ["png"], "generationImages"
    FOOLING_IMAGE = ["png"], "targetSizeImages"
    HIGH_QUALITY_IMAGE = ["png"], "highQualityImages"
    TRAFFIC_SIGN_IMAGE = ["png"], "trafficSignImages"
    SAMPLE_IMAGE = ["png", "jpg"], "sampleImagesForParent"
    PLOT = ["pdf"], "analysis"
    PLOT_DATA = ["npy"]
    METADATA = ["json"]
    CONFIG_FILE = ["json"], "config"
    TEST_DATA = ["json", "png"], "testData"

    def __new__(cls, file_extensions, folder_name=""):
        obj = object.__new__(cls)
        obj._value_ = [file_extensions, folder_name]
        return obj

    def get_all_file_extensions(self):
        return self.value[0]

    def get_first_file_extension(self):
        return self.value[0][0]

    def get_folder_name(self):
        return self.value[1]
