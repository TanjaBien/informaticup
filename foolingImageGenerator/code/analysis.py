""" This module can be used to conduct an analysis in order to improve parameter defaults and evaluate the programs value.
"""

import numpy as np
from evol.evolution import Evolution
from enums.parameter import Parameter
from util.validator import Validator
from util.figure import Figure
from util.customWarnings import CustomWarnings


class Analysis:
    CYCLES = 50
    _METADATA_EVOLUTION = "evolution_metadata"
    _METADATA_ANALYSIS = "analysis_metadata"
    _METADATA_CYCLES = "cycles"
    _METADATA_DIAGRAM_NAME = "diagram_name"
    _METADATA_ANALYSIS_TYPE = "analysis_type"
    _METADATA_KEY_FIGURES = "determine_key_figures"
    _METADATA_SIGN_DISPERSION = "determine_traffic_sign_dispersion"
    _METADATA_EXAMINE_PARAMETER = "examine_parameter"
    _METADATA_COMPARE_MUTATIONS = "compare_mutations"
    _METADATA_EXAMINED_MUTATION = "examined_mutation"
    _METADATA_PARAMETER_NAME = "parameter_name"
    _METADATA_STEP_SIZE = "step_size"
    _METADATA_INITIAL_VALUE = "initial_value"
    _METADATA_FINAL_VALUE = "final_value"
    _METADATA_MUTATIONS = "mutations"

    @staticmethod
    def run(config):
        analysis_operators = config[Validator.CONFIG_ANALYSIS]
        for analysis_operator in analysis_operators:
            analysis_function = getattr(Analysis, analysis_operator["function"])
            parameters = analysis_operator["parameters"]
            try:
                analysis_function(**parameters)
            except TypeError as e:
                Exception(f"An error occurred while running an analysis ({analysis_operator['function']})"
                          f" with the parameters: {parameters}")
                print(e)

    @staticmethod
    def examine_parameter(mutation_name, examined_parameter, initial_val, final_val, step_size, parameters,
                          diagram_name, x_label, y_label, title=None):
        """
            Returns a diagram that shows the impact of the examined parameter.

            The method tests the impact of a parameter on the efficiency of the `mutation`.
            The parameter name is specified by the `examined_parameter`.
            The method determines the average number of generations for the first fooling image found with different
            parameter values. The interval of values is defined by `initial_val` as start value,
            the `final_val` and the `step_size`.
            The value interval is shown on the x axis and the average number of generations for the
            specific parameter value on the y axis. E.g.:

            .. image:: ../_static/_images/examine_parameter.png
                :width: 250

            The diagram is saved in the plots folder as pdf.

            Parameters
            ----------
            mutation_name : str
                Name of the mutation mode which is applied to the images. Element of the Enum `Mutation`.
                The examined parameter has to be a parameter of the mutation.
                The mutation should be stated in the form Enum.MODE e.g.: `Mutation.RANDOM_PIXELS`.
            examined_parameter : str
                Name of the parameter which is supposed to be examined. Element of the `Parameter` enum.
                Parameter has to be a parameter of the `mutation`.
                The parameter should be stated in the form Enum.MODE e.g.: `Parameter.PERCENT`.
            initial_val : number
                Start of interval which is used for the examination of `parameter`.
                The initial value is included in the examination.
            final_val : number
                End of interval which is used for examination of `parameter`.
                The used interval does include this value, except
                in some cases where `step_size` is not an integer and floating point
                round-off affects the length of `parameter`.
            step_size : number
                Step size for examination of `parameter`. Spacing between values.
                For any parameter value `parameter`, this is the distance
                between two adjacent values, ``parameter[i+1] - parameter[i]``.
            parameters : {str: Any}
                Dictionary with additional parameters. `parameters` are passed to the mutation and
                shown in the diagram as text box. If the dictionary is empty,
                the default values are used but not shown in the diagram.
                The Dictionary only accepts parameters as keys,
                e.g.: {Parameter.PERCENT: 10} or {Parameter.PERCENT: Parameter.PERCENT.get_default_value()}
            diagram_name : str
                The name which is used as file name when saving the figure.
                If no alternative title is given (since parameter `title` is optional),
                the diagram name is also used as caption of the diagram.
            x_label : str
                Labeling of the x axis.
            y_label : str
                Labeling of the y axis.
            title : str, optional
                Title of the diagram. If no title is given the diagram_name is used instead.

            Returns
            -------
            No return value.

            Figure
                Shows the figure and saves it as pdf using `diagram_name` in the plots folder.
            Metadata
                Metadata of the analysis is saved as json using `diagram_name` in the plots folder.
            Data
                Data of the plot is saved as npy using `diagram_name` in the plots folder.

            See also
            --------
            compare_mutations : Draws a diagram which shows the processes of different mutations
            determine_key_figures : Draws a diagram which shows the key figures of different mutations

            Examples
            --------
            Example for the examination of the parameter `DIAMETER_TO_IMAGE_SIZE` of the mutation `DRAW_CIRCLES`.

            >>> Analysis.examine_parameter("draw_circles", "diameter_to_image_size", 10, 100, 10,
            >>>                            {"filled": True, "circles_per_round": 5},
            >>>                            "Impact of the parameter", "Parameter values", "Generation No.")

            The according `evolution_config.json` looks like this:

            .. code-block:: JSON

                {
                  "mode": "analysis",
                  "analysis": [
                                {
                                  "function": "examine_parameter",
                                  "parameters": {
                                    "mutation_name": "draw_circles",
                                    "examined_parameter": "diameter_to_image_size",
                                    "initial_val": 10,
                                    "final_val": 100,
                                    "step_size": 10,
                                    "parameters": {"filled": true, "circles_per_round": 5},
                                    "diagram_name": "Impact of the parameter",
                                    "x_label": "Parameter values",
                                    "y_label": "Generation No."
                                  }
                                }
                              ]
                }

        """
        if Validator.validate_numeric_parameter_test_params(mutation_name, examined_parameter, initial_val,
                                                            final_val, step_size):
            evolution = Evolution()
            coordinate = Analysis.__test_parameter(parameters, examined_parameter,
                                                   initial_val, final_val, step_size, mutation_name, evolution)
            parameters.pop(examined_parameter, None)
            figure = Figure.draw_plots([coordinate], diagram_name, x_label, y_label, mutation_name, parameters, title)
            evolution_metadata = evolution.get_evolution_metadata()
            metadata = Analysis.__build_metadata(evolution_metadata, Analysis._METADATA_EXAMINE_PARAMETER, diagram_name,
                                                 mutation=mutation_name, parameter_name=examined_parameter,
                                                 step_size=step_size, initial_value=initial_val, final_value=final_val)
            Figure.save_figure(figure, diagram_name, [coordinate, mutation_name], metadata)

    @staticmethod
    def compare_mutations(mutation_names, max_generation_no, diagram_name, x_label, y_label, title=None):
        """
            Returns a diagram that shows the average process of the given mutations.

            The function allows to compare multiple mutations by showing their average process.
            Every evolution has a certain number of generations specified through `max_generation_no`.
            The average percentage of confidence of the best successor in the first, second, ... nth
            generation defines the average development process, which is shown in the plot.
            The average process shows if a mutation generates fooling images very fast or very slowly
            or if there are a lot of downswings and variations.

            .. image:: ../_static/_images/compare_mutations.png
                :width: 250

            The diagrams is saved  in the plots folder as pdf.

            Parameters
            ----------
            mutation_names : [str]
                Names of mutation modes to be compared by their average process. Mutations are applied to the images.
                They are elements of the Enum Mutation and stored in an array. Multiple mutation modes are possible.
                The mutation modes should be stated in the form Enum.MODE e.g.: `[Mutation.RANDOM_PIXELS]`.
            max_generation_no : int
                Evolution stops after reaching this generation number. Therefore all evolutions have the same number
                of generations and their processes are comparable.
            diagram_name : str
                Text which is used as file name when saving the figure.
                If no alternative title is given (since parameter `title` is optional),
                the diagram name is also used as caption of the diagram.
            x_label : str
                Labeling of the x axis.
            y_label : str
                Labeling of the y axis.
            title : str, optional
                Title of the diagram. If no title is given the `diagram_name` is used instead.

            Returns
            -------
            No return value.

            Figure
                Saves the figure as pdf with the `diagram_name` in the plots folder.
            Metadata
                Metadata of the analysis is saved  as json using `diagram_name` in the plots folder.
            Data
                Data of the plot is saved as npy using `diagram_name` in the plots folder.

            See also
            --------
            examine_parameter : Draws a diagram which shows the impact of an examined parameter
            determine_key_figures : Draws a diagram which shows the key figures of different mutations

            Examples
            --------
            Example for the comparison of the mutations DRAW_CIRCLES and RANDOM.PIXELS

            >>> Analysis.compare_mutations(["draw_circles", "random_pixels"],
            >>>                            20, "Average Process", "Generation No.", "Confidence")

            The according `evolution_config.json` looks like this:

            .. code-block:: JSON

                {
                  "mode": "analysis",
                  "analysis": [
                                {
                                  "function": "compare_mutations",
                                  "parameters": {
                                    "mutation_names": ["draw_circles", "random_pixels"],
                                    "max_generation_no": 20,
                                    "diagram_name": "Average Process",
                                    "x_label": "Generation No.",
                                    "y_label": "Confidence"
                                  }
                                }
                              ]
                }

        """
        if Validator.validate_compare_mutations(mutation_names, max_generation_no):
            evolution = Evolution()
            mutation_labels = []
            coordinates = []
            evolution_metadata = []
            for mutation_name in mutation_names:
                print(f"Mutation: {mutation_name}")
                mutation_labels.append(mutation_name)
                processes_mean, evolution_metadata_for_mutation = \
                    Analysis.__get_average_process(mutation_name, max_generation_no, evolution)
                print(processes_mean)
                coordinates.append((list(range(max_generation_no)), processes_mean))
                evolution_metadata.append(evolution_metadata_for_mutation)
            print(mutation_labels)
            figure = Figure.draw_plots(coordinates, diagram_name, x_label, y_label, mutation_labels, title=title)
            metadata = Analysis.__build_metadata(evolution_metadata, Analysis._METADATA_COMPARE_MUTATIONS, diagram_name,
                                                 mutations=mutation_names, max_generation_no=max_generation_no)
            Figure.save_figure(figure, diagram_name, [coordinates, mutation_names], metadata)

    @staticmethod
    def determine_key_figures(mutation_names, max_generation_no, diagram_name, bar_labels):
        """
            Returns a bar chart that shows the key figures of the given mutations.

            The function allows to compare multiple mutations by showing their key figures.
            There are three key figures which are to be determined. First, the average number of generations
            which are needed to generate the first fooling image.
            Second, the average number of fooling images which are generated in `max_generation_no` generations.
            And finally the number of downswings in `max_generation_no` generations. A downswing means that
            the parent is fitter than the best successor.
            The key figures are displayed in a bar so it is possible to directly compare multiple mutations. E.g.:

            .. image:: ../_static/_images/key_figures.png
                :width: 250

            The diagrams is saved  in the plots folder as pdf.

            Parameters
            ----------
            mutation_names : [str]
                Names of mutations modes to be compared by their key figures. Mutations modes are applied to the images.
                They are elements of the Enum Mutation and stored in an array. Multiple mutation modes are possible.
                The mutation modes should be stated in the form Enum.MODE e.g.: `[Mutation.RANDOM_PIXELS]`.
            max_generation_no : int
                Evolution stops after reaching this generation number. Therefore all evolutions have the same number
                of generations and are comparable.
            diagram_name : str
                The name which is used as file name when saving the figure.
                If no alternative title is given (since parameter `title` is optional),
                the diagram name is also used as caption of the diagram.
            bar_labels : Tuple(str)
                Labeling for the bar. The first bar is for the number of generations until the first fooling image is
                generated, the second shows the number of fooling images generated in `max_generation_no` and the last
                symbolizes the number of downswings in in `max_generation_no`.

            Returns
            -------
            No return value.

            Figure
                Shows the figure and saves it as pdf using `diagram_name` in the plots folder.
            Metadata
                Metadata of the analysis is saved as json using `diagram_name` in the plots folder.
            Data
                Data of the plot is saved as npy using `diagram_name` in the plots folder.

            See also
            --------
            examine_parameter : Draws a diagram which shows the impact of a examined parameter
            compare_mutations : Draws a diagram which shows the processes of different mutation modes

            Examples
            --------
            Example of the comparison of the key figures of the mutations `DRAW_CIRCLES` and `RANDOM_PIXELS`.

            >>> Analysis.determine_key_figures(["draw_circles", "point_symmetric_polygons", "color_grid"],
            >>>                                50, "Key figures",
            >>>                                ("first fooling image", "fooling images", "downswings"))

            The according `evolution_config.json` looks like this:

            .. code-block:: JSON

                {
                  "mode": "analysis",
                  "analysis": [
                                {
                                  "function": "determine_key_figures",
                                  "parameters": {
                                    "mutation_names": ["draw_circles", "point_symmetric_polygons", "color_grid"],
                                    "max_generation_no": 50,
                                    "diagram_name": "Key figures",
                                    "bar_labels": ["first fooling image", "fooling images", "downswings"]
                                  }
                                }
                              ]
                }

        """
        if Validator.validate_determine_key_figures(mutation_names, max_generation_no):
            evolution = Evolution()
            data_sets = []
            evolution_metadata = []
            for mutation_name in mutation_names:
                mutation_label = Figure.beautify_string(mutation_name)
                print(f"Mutation: {mutation_label}")
                generation_nr, evolution_metadata_for_mutation = \
                    Analysis.__generation_nr_fooling_img(mutation_name, evolution)
                print(f"Generation No.: {generation_nr}")
                number_fooling_images, number_downswings = \
                    Analysis.__number_fooling_images_and_downswings(mutation_name, max_generation_no, evolution)
                print(f"No. Fooling images: {number_fooling_images}")
                print(f"No. Downswings: {number_downswings}")
                data_set = ([generation_nr, number_fooling_images, number_downswings], mutation_label)
                data_sets.append(data_set)
                evolution_metadata.append(evolution_metadata_for_mutation)
            figure, diagram_name = Figure.draw_bar_chart(data_sets, diagram_name, bar_labels)
            metadata = Analysis.__build_metadata(evolution_metadata, Analysis._METADATA_KEY_FIGURES, diagram_name,
                                                 mutations=mutation_names, max_generation_no=max_generation_no)
            Figure.save_figure(figure, diagram_name, data_sets, metadata)

    @staticmethod
    def determine_sign_dispersion(mutation_name, n, diagram_name, title=None):
        """
            Returns a bar chart that shows the occurrences of the traffic signs.

            The result of an evolution is an fooling image for a certain traffic sign.
            To determine which traffic signs are likely to be a result of a mutation, the function
            shows the traffic signs' dispersion of them in a bar chart. The given Mutation is applied
            during `n` evolutions. Hence the sum of all occurrences is n. E.g.:

            .. image:: ../_static/_images/traffic_sign_dispersion.png
                :width: 250

            The chart is saved in the plots folder as pdf.

            Parameters
            ----------
            mutation_name : Str
                Name of the mutation mode which is applied to the pictures. Element of the Enum Mutation.
                The bar chart shows the traffic sign dispersion for the specified Mutation.
                The mutation mode should be stated in the form Enum.MODE e.g.: `Mutation.RANDOM_PIXELS`.
            n : int
                Total number of evolutions which generate the fooling images. The traffic sign of the first
                generated fooling image during each evolution is used to determine the occurrences.
                The sum of all occurrences is n.
            diagram_name : str
                The name which is used as file name when saving the figure.
                If no alternative title is given (since parameter `title` is optional),
                the diagram name is also used as caption of the diagram.
            title : str, optional
                Title of the diagram. If no title is given the diagram_name is used instead.

            Returns
            -------
            No return value.

            Figure
                Shows the figure and saves it as pdf using `diagram_name` in the plots folder.
            Metadata
                Metadata of the analysis is saved as json using `diagram_name` in the plots folder.
            Data
                Data of the plot is saved as npy using `diagram_name` in the plots folder.

            See also
            --------
            determine_key_figures : Draws a diagram which shows the key figures of different mutations

            Examples
            --------
            Example for investigation of the traffic sign dispersion of the mutation `DRAW_CIRCLES`.

            >>> Analysis.determine_sign_dispersion("draw_circles", 10, "Dispersion circles with n = 10")

            The according `evolution_config.json` looks like this:

            .. code-block:: JSON

                {
                  "mode": "analysis",
                  "analysis": [
                                {
                                  "function": "determine_sign_dispersion",
                                  "parameters": {
                                    "mutation_name": "draw_circles",
                                    "n": 100,
                                    "diagram_name": "Dispersion circles with n = 10"
                                  }
                                }
                              ]
                }
        """
        evolution = Evolution()
        data_sets, x_labels = Analysis.__traffic_sign_dispersion(mutation_name, n, evolution)
        print(data_sets)
        print(x_labels)
        figure, diagram_name = Figure.draw_bar_chart([data_sets], diagram_name, x_labels, title)
        evolution_metadata = evolution.get_evolution_metadata()
        metadata = Analysis.__build_metadata(evolution_metadata, Analysis._METADATA_SIGN_DISPERSION, diagram_name,
                                             mutation=mutation_name, evolutions=n)
        Figure.save_figure(figure, diagram_name, data_sets, metadata)

    @staticmethod
    def __test_parameter(parameters, examined_parameter, initial_val, final_val, step_size, mutation_name, evolution):
        if final_val < initial_val:
            CustomWarnings.print_warning("Final value is greater than the initial value.")
        xs = []
        ys = []
        parameter = initial_val
        while parameter <= final_val:
            print("Parameter value: " + str(parameter))
            parameters[examined_parameter] = parameter
            xs.append(parameter)
            y, metadata = Analysis.__generation_nr_fooling_img(mutation_name, evolution, parameters)
            ys.append(y)
            parameter += step_size
            print(ys)
        return xs, ys

    @staticmethod
    def __generation_nr_fooling_img(mutation_name, evolution, parameters={}):
        cycle = []
        for i in range(Analysis._CYCLES):
            evolution.start(mutation_name=mutation_name, **parameters)
            cycle.append(evolution.get_generation_no())
        evolution_metadata = evolution.get_evolution_metadata()
        return np.average(cycle), evolution_metadata

    @staticmethod
    def __number_fooling_images_and_downswings(mutation_name, max_generation_no, evolution):
        number_fooling_images = []
        number_downswings = []
        for i in range(Analysis._CYCLES):
            evolution.start(mutation_name, max_generation_no=max_generation_no)
            number_fooling_images.append(evolution.get_number_fooling_images())
            number_downswings.append(evolution.get_number_downswings())
        return np.average(number_fooling_images), np.average(number_downswings)

    @staticmethod
    def __get_average_process(mutation_name, max_generation_no, evolution):
        processes = []
        for i in range(Analysis._CYCLES):
            evolution.start(mutation_name, max_generation_no=max_generation_no)
            processes.append(np.array(evolution.get_process()))
        processes = np.array(processes)
        evolution_metadata = evolution.get_evolution_metadata()
        return processes.mean(axis=0), evolution_metadata

    @staticmethod
    def __traffic_sign_dispersion(mutation_name, n, evolution):
        classes = {}
        occurrences = []
        labels = []
        for i in range(n):
            evolution.start(mutation_name)
            traffic_sign = evolution.get_parent_traffic_sign()
            old_value = classes.pop(traffic_sign, 0)
            classes[traffic_sign] = old_value + 1
        classes = [(k, classes[k]) for k in sorted(classes, key=classes.get, reverse=True)]
        print(classes)
        for (traffic_sign, occurrence) in classes:
            occurrences.append(occurrence)
            labels.append(traffic_sign)
        return [occurrences, mutation_name], labels

    @staticmethod
    def __build_metadata(evolution_metadata, analysis_type, diagram_name, **parameters):
        analysis_metadata = Analysis.__build_analysis_metadata(diagram_name, analysis_type, **parameters)
        if analysis_type is Analysis._METADATA_EXAMINE_PARAMETER:
            parameter_name = parameters[Analysis._METADATA_PARAMETER_NAME]
            parameter_range = []
            param = parameters[Analysis._METADATA_INITIAL_VALUE]
            while param <= parameters[Analysis._METADATA_FINAL_VALUE]:
                parameter_range.append(param)
                param += parameters[Analysis._METADATA_STEP_SIZE]
            range_as_list = [*parameter_range]
            evolution_metadata[Evolution.METADATA_MUTATION][parameter_name] = range_as_list
        metadata = {Analysis._METADATA_ANALYSIS: analysis_metadata,
                    Analysis._METADATA_EVOLUTION: evolution_metadata}
        return metadata

    @staticmethod
    def __build_analysis_metadata(diagram_name, analysis_type, **parameters):
        analysis_metadata = {Analysis._METADATA_DIAGRAM_NAME: diagram_name,
                             Analysis._METADATA_ANALYSIS_TYPE: analysis_type}
        if not analysis_type == Analysis._METADATA_SIGN_DISPERSION:
            analysis_metadata[Analysis._METADATA_CYCLES] = Analysis._CYCLES
        for key in parameters:
            if key is Analysis._METADATA_MUTATIONS:
                mutation_names = []
                for mutation in parameters[key]:
                    mutation_names.append(mutation)
                analysis_metadata[key] = mutation_names
            else:
                analysis_metadata[key] = parameters[key]
        return analysis_metadata
