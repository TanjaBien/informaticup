"""This module implements the class Evolution.
"""

from evol.generation import Generation
from util.validator import Validator
from enums.mutation import Mutation
from enums.parameter import Parameter


class Evolution:
    _METADATA_FILE = "file"
    _METADATA_MODE = "mode"
    METADATA_MUTATION = "mutation"
    _METADATA_PARENT = "parent"

    _MUTATION_NAME = Parameter.MUTATION_NAME.get_default_value()
    _SUCCESSORS_COUNT = Parameter.SUCCESSORS_COUNT.get_default_value()
    _MAX_GENERATION_NO = Parameter.MAX_GENERATION_NO.get_default_value()
    _ALLOW_DOWNSWING = Parameter.ALLOW_DOWNSWING.get_default_value()
    _HIGH_QUALITY = Parameter.HIGH_QUALITY.get_default_value()
    _SHOW_EVOLUTION_PROCESS = Parameter.SHOW_EVOLUTION_PROCESS.get_default_value()
    _CONFIDENCE_THRESHOLD = Parameter.CONFIDENCE_THRESHOLD.get_default_value()

    def __init__(self):
        self._process = []
        self._generation_number = 0
        self._map_parent_fitness = (None, 0)
        self._generated_fooling_image = False
        self._number_fooling_images = 0
        self._number_downswings = 0
        self._evolution_metadata = {}
        self._confidence_threshold = 0
        self._parent_path = None

    def start(self, mutation_name=_MUTATION_NAME, successors_count=_SUCCESSORS_COUNT,
              max_generation_no=_MAX_GENERATION_NO, allow_downswing=_ALLOW_DOWNSWING,
              high_quality=_HIGH_QUALITY, confidence_threshold=_CONFIDENCE_THRESHOLD,
              show_evolution_process=_SHOW_EVOLUTION_PROCESS, **parameters):
        """
            Starts an evolution and runs it until the given conditions are met.

            The method starts an evolution. It will run until either a maximum number of generations is reached
            (if given as `max_generation_no`), a fooling image has been generated or timeout is exceeded.


            Parameters
            ----------
            mutation_name : String, optional
                Name of the mutation mode used for developing a generation out of a parent.
            successors_count : int, optional
                Number of individuals per generation.
            max_generation_no : int, optional
                Maximum number of generations until evolution is aborted.
            allow_downswing: bool, optional
                Determines a criterion for choosing the parent of the next generation. If True, the next parent will
                always be an individual of the current generation, even if it is inferior to its prior parent. If False,
                no worsening is allowed. Thus a parent could survive its children, if it is better than them.
            high_quality: bool, optional
                Determines whether each fooling image will be saved in its original size in addition to the 64x64-image,
                which is sent to the server.
            confidence_threshold: [90, 100], optional
                Determines the threshold for fooling image. The confidence of an image must be higher than this value
                in order to be categorized as fooling image.
            show_evolution_process: bool, optional
                Determines whether the process of the evolution is printed to the console. If set to true, for each
                generation the following information will be printed: generation number, confidence of the best
                individual and (in case `allow_downswing` is set to True) the confidence of the current parent.
                By default, this parameter is set to True when running the evolution without GUI,
                else it will be set to False.
            parameters: keyword arguments
                Optional parameters, which depend on the chosen mutation mode and whether a parent is set.
                See GeneticOperator.mutate and Generation.generate_successors for more information.

            See also
            --------
            prepare_evolution: prepares the evolution before its first generation
            evolve: develops the next generation
        """
        mutation = Mutation(mutation_name)
        mutation, successors_count, max_generation_no, allow_downswing, high_quality, confidence_threshold, \
        validated_parameters = self.prepare_evolution(mutation, successors_count, max_generation_no,
                                                      allow_downswing, high_quality, confidence_threshold,
                                                      show_evolution_process, **parameters)
        if max_generation_no:
            while self._generation_number < max_generation_no:
                self.evolve(successors_count, mutation, allow_downswing, high_quality, show_evolution_process,
                            **validated_parameters)
        else:
            while not self._generated_fooling_image and \
                    self._generation_number < self.__determine_timeout(successors_count):
                self.evolve(successors_count, mutation, allow_downswing, high_quality, show_evolution_process,
                            **validated_parameters)

    def prepare_evolution(self, mutation, successors_count, max_generation_no, allow_downswing, high_quality,
                          confidence_threshold, show_evolution_process, **parameters):
        """
            Prepares the evolution in order to be started.

            The method resets the evolution instance to its original state, so a new evolution can be started.
            The given parameters for the new evolution are validated and corrected to default values if ill-stated.
            The parameters are set as metadata for later analysis.


            Parameters
            ----------
            mutation : Mutation, optional
                Mutation mode used for developing a generation out of a parent.
            successors_count : int, optional
                Number of individuals per generation.
            max_generation_no : int, optional
                Maximum number of generations until evolution is aborted.
            allow_downswing: bool, optional
                Determines a criterion for choosing the parent of the next generation. If True, the next parent will
                always be an individual of the current generation, even if it is inferior to its prior parent. If False,
                no worsening is allowed. Thus a parent could survive its children, if it is better than them.
            high_quality: bool, optional
                Determines whether each fooling image will be saved in its original size in addition to the 64x64-image,
                which is sent to the server.
            confidence_threshold: [90, 100], optional
                Determines the threshold for fooling image. The confidence of an image must be higher than this value
                in order to be categorized as fooling image.
            show_evolution_process: bool, optional
                Determines whether the process of the evolution is printed to the console. If set to true, for each
                generation the following information will be printed: generation number, confidence of the best
                individual and (in case `allow_downswing` is set to True) the confidence of the current parent.
                By default, this parameter is set to True when running the evolution without GUI,
                else it will be set to False.
            parameters: keyword arguments
                Optional parameters, which depend on the chosen mutation mode and whether a parent is set.
                See GeneticOperator.mutate and Generation.generate_successors for more information.


            Returns
            -------
            mutation
                validated input parameter `mutation`
            successors_count
                validated input parameter `successors_count`
            max_generation_no
                validated input parameter `max_generation_no`
            allow_downswing
                validated input parameter `allow_downswing`
            high_quality
                validated input parameter `high_quality`
            confidence_threshold
                validated input parameter `confidence_threshold`
            validated_parameters
                validated input parameter `parameters`
        """
        self.__big_bang(confidence_threshold)
        mutation, validated_parameters, (successors_count, max_generation_no, allow_downswing,
                                         high_quality, confidence_threshold, show_evolution_process)\
            = Validator.validate_evolution(mutation, parameters, successors_count=successors_count,
                                           max_generation_no=max_generation_no, allow_downswing=allow_downswing,
                                           high_quality=high_quality, confidence_threshold=confidence_threshold,
                                           show_evolution_process=show_evolution_process)
        self.__add_metadata(successors_count, mutation, allow_downswing, max_generation_no, **validated_parameters)
        if show_evolution_process:
            print("Mutation: " + mutation.value)
        return (mutation, successors_count, max_generation_no, allow_downswing, high_quality, confidence_threshold,
                validated_parameters)

    def evolve(self, successors_count, mutation, allow_downswing, high_quality, show_evolution_process, **parameters):
        """
            Brings the evolution one step further, respectively generates the next generation.

            The method takes the current parent and develops the next generation on the basis of the current parent.
            Out of the generated individuals the best one (or fittest) is chosen as parent. If the chosen individual
            is worse than the previous parent, it is replaced by the previous parent if `allow_downswing` is set to
            False. The method also checks for each individual whether it fulfills the criterion for a fooling image.


            Parameters
            ----------
            successors_count : int, optional
                Number of individuals per generation.
            mutation : Mutation, optional
                Mutation mode used for developing a generation out of a parent.
            allow_downswing: bool
                Determines a criterion for choosing the parent of the next generation. If True, the next parent will
                always be an individual of the current generation, even if it is inferior to its prior parent. If False,
                no worsening is allowed. Thus a parent could survive its children, if it is better than them.
            high_quality: bool, optional
                Determines whether each fooling image will be saved in its original size in addition to the 64x64-image,
                which is sent to the server.
            show_evolution_process: bool, optional
                Determines whether the process of the evolution is printed to the console. If set to true, for each
                generation the following information will be printed: generation number, confidence of the best
                individual and (in case `allow_downswing` is set to True) the confidence of the current parent.
                By default, this parameter is set to True when running the evolution without GUI,
                else it will be set to False.
            parameters: keyword arguments
                Optional parameters which depend on the chosen mutation and whether a parent is set.
                See GeneticOperator.mutate and Generation.generate_successors for more information.
        """
        generation = Generation(self._generation_number, successors_count, self._evolution_metadata)
        parent = self._map_parent_fitness[0]
        generation.generate_successors(mutation, high_quality, parent, **parameters)
        fittest = generation.select_fittest()
        if show_evolution_process:
            print("Generation: " + str(self._generation_number))
            print("Best Successor: " + str(fittest[1]))
            print("Traffic sign: " + fittest[0].get_traffic_sign())
        if allow_downswing:
            if self.__is_parent_fitter(fittest):
                self._number_downswings += 1
            self._map_parent_fitness = fittest
        else:
            tmp = Generation.max_fitness(fittest, self._map_parent_fitness)
            if show_evolution_process:
                print("Parent: " + str(self._map_parent_fitness[1]))
            self._map_parent_fitness = tmp
        if show_evolution_process:
            print("")
        self._generation_number += 1
        self._generated_fooling_image = generation.number_fooling_images > 0
        self._number_fooling_images += generation.number_fooling_images
        self._process.append(self.get_parent_fitness() * 100)

    def __big_bang(self, confidence_threshold):
        self.__init__()
        self._confidence_threshold = confidence_threshold

    def __is_parent_fitter(self, fittest_successor):
        tmp = Generation.max_fitness(fittest_successor, self._map_parent_fitness)
        return tmp == self._map_parent_fitness

    @staticmethod
    def __determine_timeout(successors_count):
        second = 1
        minute = 60 * second
        hour = 60 * minute
        return hour / successors_count

    def get_parent_fitness(self):
        return self._map_parent_fitness[1]

    def get_generation_no(self):
        return self._generation_number

    def get_number_fooling_images(self):
        return self._number_fooling_images

    def get_number_downswings(self):
        return self._number_downswings

    def get_process(self):
        return self._process

    def get_parent_traffic_sign(self):
        return self._map_parent_fitness[0].traffic_sign

    def get_parent_fooling_image(self):
        return self._map_parent_fitness[0]

    def is_fooling_image_generated(self):
        return self._generated_fooling_image

    def get_evolution_metadata(self):
        return self._evolution_metadata

    def __add_metadata(self, successors_count, mutation, allow_downswing, max_generation_no, **parameters):
        self._evolution_metadata[Parameter.SUCCESSORS_COUNT.get_name()] = successors_count
        self._evolution_metadata[Parameter.ALLOW_DOWNSWING.get_name()] = allow_downswing
        self._evolution_metadata[Parameter.MAX_GENERATION_NO.get_name()] = max_generation_no
        self._evolution_metadata[Parameter.CONFIDENCE_THRESHOLD.get_name()] = self._confidence_threshold
        self._evolution_metadata[Evolution._METADATA_PARENT] = Parameter.get_default_parent_parameters()
        self._evolution_metadata[Evolution.METADATA_MUTATION] = {}
        self.__add_parent_metadata(**parameters)
        self.__add_mutation_metadata(mutation, **parameters)

    def __add_parent_metadata(self, **parameters):
        if Parameter.PARENT_PATH.get_name() in parameters:
            if parameters[Parameter.PARENT_PATH.get_name()] is not None:
                self._evolution_metadata[Evolution._METADATA_PARENT][Parameter.PARENT_PATH.get_name()] =\
                    parameters[Parameter.PARENT_PATH.get_name()]
            else:
                parent_default_parameters = Parameter.get_default_parent_parameters()
                for key, value in parent_default_parameters.items():
                    if key in parameters:
                        self._evolution_metadata[Evolution._METADATA_PARENT][key] = parameters[key]
                    else:
                        self._evolution_metadata[Evolution._METADATA_PARENT][key] = value

    def __add_mutation_metadata(self, mutation, **parameters):
        mutation_default_parameters = Parameter.get_default_parameters_for_mutation(mutation)
        self._evolution_metadata[Evolution.METADATA_MUTATION][Evolution._METADATA_MODE] = mutation.value
        for key, value in mutation_default_parameters.items():
            if key in parameters:
                self._evolution_metadata[Evolution.METADATA_MUTATION][key] = parameters[key]
            else:
                self._evolution_metadata[Evolution.METADATA_MUTATION][key] = value
