"""This package includes all classes necessary to represent an evolution.

Submodules
==========

.. autosummary::

    evolution
    generation
    geneticOperator
    individual
"""
