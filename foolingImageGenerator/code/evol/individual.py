"""This module implements the class Individual.
"""

from util.image import Image
from util.fileManager import FileManager as fm


class Individual(Image):
    METADATA_GENERATION_NO = "generation_number"
    METADATA_INDIVIDUAL_NO = "individual_number"
    METADATA_TRAFFIC_SIGN = "traffic_sign"
    METADATA_CONFIDENCE = "confidence"

    def __init__(self, generation_no=0, individual_no=0, given_image_path=None):
        self.image = Image.__init__(self, given_image_path)
        self.image_name = fm.build_name(fm.IMAGE_PREFIX, generation_no, individual_no)
        self.image_metadata = {}
        self.generation_no = generation_no
        self.individual_no = individual_no
        self.traffic_sign = None
        self.confidence = None
        self.__add_metadata()

    def add_request_data_to_metadata(self, traffic_sign, confidence):
        self.traffic_sign = traffic_sign
        self.confidence = confidence * 100
        self.image_metadata[self.METADATA_TRAFFIC_SIGN] = traffic_sign
        self.image_metadata[self.METADATA_CONFIDENCE] = confidence

    def is_fooling_image(self, confidence_threshold):
        return self.confidence and self.confidence >= confidence_threshold

    def __add_metadata(self):
        self.image_metadata[self.METADATA_GENERATION_NO] = self.generation_no
        self.image_metadata[self.METADATA_INDIVIDUAL_NO] = self.individual_no

    def get_traffic_sign(self):
        return self.traffic_sign
