"""This module implements the class GeneticOperator. It's a collection of different operators, currently this includes only mutation operators.
"""

import random
import math
import cv2
import sys
import numpy as np
from evol.individual import Individual
from util.image import Image
from enums.mutation import Mutation
from enums.parameter import Parameter


class GeneticOperator:
    # pixels
    PERCENT = Parameter.PERCENT.get_default_value()
    # circles
    DIAMETER_TO_IMAGE_SIZE = Parameter.DIAMETER_TO_IMAGE_SIZE.get_default_value()
    RATIO_MAX_LINE_THICKNESS_TO_RADIUS = Parameter.RATIO_MAX_LINE_THICKNESS_TO_RADIUS.get_default_value()
    CIRCLES_PER_ROUND = Parameter.CIRCLES_PER_ROUND.get_default_value()
    FILLED = Parameter.FILLED.get_default_value()
    # rectangles
    RECTANGLES_PER_ROUND = Parameter.RECTANGLES_PER_ROUND.get_default_value()
    MAX_LINE_THICKNESS_FACTOR = Parameter.MAX_LINE_THICKNESS_FACTOR.get_default_value()
    # point symmetric polygons
    POLYGONS_PER_ROUND = Parameter.POLYGONS_PER_ROUND.get_default_value()
    VERTICES = Parameter.VERTICES.get_default_value()
    AREAS = Parameter.AREAS.get_default_value()
    # color/ color gradient addition
    COLUMNS_AND_ROWS = Parameter.COLUMNS_AND_ROWS.get_default_value()
    GRADIENT = Parameter.GRADIENT.get_default_value()
    MAX_RADIAL_COORDINATE = math.sqrt((Image.SIZE / 2) * (Image.SIZE / 2) * 2)
    # general
    MIN_LINE_THICKNESS = -1

    # metadata
    METADATA_LINE_THICKNESS = "line_thickness"

    @staticmethod
    def mutate(mutation, origin_img, generation_no, individual_no, **mutation_parameters):
        """
            Mutates an individual.

            The method takes an image (`origin_image`) and mutates a copy of it by using the
            specified mutation mode (`mutation`).


            Parameters
            ----------
            mutation : Mutation mode
                Mutation mode which is applied on the picture. Element of the Enum Mutation.
                The `**parameters` have to be parameters of the mutation mode used.
                The mutation mode should be written in the form Enum.MODE e.g.: `Mutation.RANDOM_PIXELS`.
            origin_img : Individual
                Individual (image) which will be altered.
            generation_no : int
                Number of generation, which the altered image belongs to.
            individual_no : int
                Number of individual (image) in the current generation.
            mutation_parameters: dictionary, optional
                Parameters which are specific for the mutation mode used. See the specific method of the specified
                mutation mode for more information about these parameters.

            Returns
            -------
            img: Individual
                The altered image. It is returned by calling the method for the specified mutation.

            See also
            --------
            _GeneticOperator__change_random_pixels: Specific Mutation. Set pixels to a random color.
            _GeneticOperator__draw_random_circles: Specific Mutation. Draw circles onto the image.
            _GeneticOperator__draw_random_rectangles: Specific Mutation. Draw rectangles onto the image.
            _GeneticOperator__draw_point_symmetric_polygons: Specific Mutation. Split the image into radial areas and
                draw polygons onto the image.
            _GeneticOperator__color_grid: Specific Mutation. Lays a colored grid over the image.
        """
        mutations = {Mutation.RANDOM_PIXELS: GeneticOperator.__change_random_pixels,
                     Mutation.DRAW_CIRCLES: GeneticOperator.__draw_random_circles,
                     Mutation.DRAW_RECTANGLES: GeneticOperator.__draw_random_rectangles,
                     Mutation.POINT_SYMMETRIC_POLYGONS: GeneticOperator.__draw_point_symmetric_polygons,
                     Mutation.COLOR_GRID: GeneticOperator.__color_grid}
        try:
            img = mutations[mutation](origin_img, generation_no, individual_no, **mutation_parameters)
            img.img = Image.satisfy_float_value_range(img.img)
            return img
        except TypeError as e:
            Exception(f"An error occurred in genetic operator {mutation.value}")
            print(e)
            sys.exit(1)

    @staticmethod
    def __get_parameters_for_mutation(mutation):
        params_for_mutation = {Mutation.RANDOM_PIXELS: [Parameter.PERCENT],
                               Mutation.DRAW_CIRCLES: [Parameter.CIRCLES_PER_ROUND, Parameter.DIAMETER_TO_IMAGE_SIZE,
                                                       Parameter.FILLED,
                                                       Parameter.RATIO_MAX_LINE_THICKNESS_TO_RADIUS],
                               Mutation.DRAW_RECTANGLES: [Parameter.RECTANGLES_PER_ROUND,
                                                          Parameter.MAX_LINE_THICKNESS_FACTOR,
                                                          Parameter.FILLED],
                               Mutation.POINT_SYMMETRIC_POLYGONS: [Parameter.VERTICES, Parameter.AREAS,
                                                                   Parameter.POLYGONS_PER_ROUND],
                               Mutation.COLOR_GRID: [Parameter.COLUMNS_AND_ROWS, Parameter.GRADIENT]}
        return params_for_mutation[mutation]

    @staticmethod
    def __build_range_string(minimum, maximum):
        return "[" + str(minimum) + ", " + str(maximum) + "]"

    @staticmethod
    def __change_random_pixels(origin_img, generation_no, individual_no, percent=PERCENT):
        """
            Changes each pixel of an image to a random color with probability `percent`.

            The method takes an image and alters it, thus creates a new image out of another image.
            Each pixel of the original image is set to a random color with a possibility of `percent` percent.
            If the image is bigger than the target size (64x64), multiple pixels will be set at the same time.
            E.g. for a 128x128 image instead of 1 pixel a square of 2x2 pixels will be set to a random color.
            E.g.:


                .. figure:: ../_static/_images/random_pixels.png
                    :width: 200
                    :figwidth: 250

                    Example with following parameters: percent = 10


            Parameters
            ----------
            origin_img : Individual
                Individual which will be altered
            generation_no : int
                Number of generation, which the altered image belongs to.
            individual_no : int
                Number of individual (image) in the current generation.
            percent: [0,100], optional
                Probability, which is used for each pixel, to determine whether it will be changed.

            Returns
            -------
            img: Individual
                The altered image.
        """
        img = Individual(generation_no, individual_no)
        img.img = np.copy(origin_img.img)
        pixel_width = int(Image.SIZE / Image.SIZE_FINAL)
        for i in range(0, Image.SIZE, pixel_width):
            for j in range(0, Image.SIZE, pixel_width):
                random_number = random.randint(1, 101)
                if 0 < random_number <= percent:
                    img.set_random_color_for_pixel(i, j, pixel_width)
        return img

    @staticmethod
    def __draw_random_circles(origin_img, generation_no, individual_no, diameter_to_image_size=DIAMETER_TO_IMAGE_SIZE,
                              circles_per_round=CIRCLES_PER_ROUND, filled=FILLED):
        """
            Draws random circles onto an image.

            The method takes an image (`origin_image`), copies it and draws random circles onto it.
            The number of circles, which are drawn, is defined by `circles_per_round`.
            The line thickness is set randomly, if `filled` is not specified. If specified, the circles will be
            completely filled with color. The color of the circles is set randomly and applies to all circles.
            The radius is set randomly but can be limited to a ratio of image size, specified by
            `diameter_to_image_size`.
             E.g.:


                .. figure:: ../_static/_images/draw_circles.png
                    :width: 200
                    :figwidth: 250

                    Example with following parameters: circles_per_round = 3, diameter_to_image_size = 70,
                    filled = True


            Parameters
            ----------
            origin_img : Individual
                Individual which will be altered
            generation_no : int
                Number of generation, which the altered image belongs to.
            individual_no : int
                Number of individual (image) in the current generation.
            diameter_to_image_size: [0.0, 100.0], optional
                Number which determines the maximum radius with respect to the image size.
                The maximum value to be defined is 1, which means a radius equal to the image width.
            circles_per_round: int, optional
                Number of circles which are drawn onto the image.
            filled: bool, optional
                Determines whether the circles are filled (True) or line thickness is set randomly (False).

            Returns
            -------
            img: Individual
                The altered image.
        """
        img = Individual(generation_no, individual_no)
        img.img = np.copy(origin_img.img)

        max_diameter = diameter_to_image_size / 100 * Image.SIZE
        if not filled:
            max_radius = round(max_diameter / (2 + GeneticOperator.RATIO_MAX_LINE_THICKNESS_TO_RADIUS))
        else:
            max_radius = round(max_diameter / 2)
        color = Image.get_random_color()
        for i in range(0, circles_per_round):
            radius = random.randint(1, max_radius)
            line_thickness = random.randint(GeneticOperator.MIN_LINE_THICKNESS,
                                            int(radius * GeneticOperator.RATIO_MAX_LINE_THICKNESS_TO_RADIUS))
            if filled:
                line_thickness = -1
            random_x = random.randint(0, Image.SIZE)
            random_y = random.randint(0, Image.SIZE)
            cv2.circle(img.img, (random_x, random_y), radius, color, line_thickness, lineType=cv2.LINE_AA)
        return img

    @staticmethod
    def __draw_random_rectangles(origin_img, generation_no, individual_no, rectangles_per_round=RECTANGLES_PER_ROUND,
                                 filled=FILLED):
        """
                Draws random rectangles onto an image.

                The method takes an image (`origin_image`), copies it and draws random rectangles onto it.
                The number of rectangles, which are drawn, is defined by `rectangles_per_round`. The line thickness
                is set randomly, if `filled` is not specified. If specified the rectangles will be completely filled
                with color. The color of the rectangles is set randomly and applies to all rectangles. The size and
                position of each rectangle is set randomly. E.g.:


                .. figure:: ../_static/_images/draw_rectangles.png
                    :width: 200
                    :figwidth: 250

                    Example with following parameters: rectangles_per_round = 1, filled = True


                Parameters
                ----------
                origin_img : Individual
                    Individual which will be altered
                generation_no : int
                    Number of generation, which the altered image belongs to.
                individual_no : int
                    Number of individual (image) in the current generation.
                rectangles_per_round: int, optional
                    Number of rectangles which are drawn onto the image.
                filled: bool, optional
                    Determines whether the rectangles are filled (True) or line thickness is set randomly (False).

                Returns
                -------
                img: Individual
                    The altered image.
            """
        img = Individual(generation_no, individual_no)
        img.img = np.copy(origin_img.img)
        color = Image.get_random_color()
        max_line_thickness = int(GeneticOperator.MAX_LINE_THICKNESS_FACTOR * Image.SIZE)
        for i in range(0, rectangles_per_round):
            line_thickness = random.randint(GeneticOperator.MIN_LINE_THICKNESS, max_line_thickness)
            if filled:
                line_thickness = -1
            point1 = Image.get_random_point()
            point2 = Image.get_random_point()
            cv2.rectangle(img.img, point1, point2, color, line_thickness)
        return img

    @staticmethod
    def __draw_point_symmetric_polygons(origin_img, generation_no, individual_no, vertices=VERTICES, areas=AREAS,
                                        polygons_per_round=POLYGONS_PER_ROUND):
        """
                Draws point symmetric polygons onto an image.

                The method takes an image (`origin_image`), copies it and draws polygons onto it. The image is divided
                into several radial areas around the image center. The number of vertices is defined by `vertices`.
                The parameter `polygons_per_round` determines how many polygons will be drawn per area. The areas are
                point symmetric to the image center. Thus each point can be described by polar coordinates. The color
                of the polygons is set randomly and applies to all polygons. E.g.:


                .. figure:: ../_static/_images/point_symmetric_polygons.png
                    :width: 200
                    :figwidth: 250

                    Example with following parameters: areas = 4, vertices = 3, polygons_per_round = 1


                Parameters
                ----------
                origin_img : Individual
                    Individual which will be altered
                generation_no : int
                    Number of generation, which the altered image belongs to.
                individual_no : int
                    Number of individual (image) in the current generation.
                vertices: int, optional
                    Number of vertices per polygon. For values >= 3, polygons are drawn onto the image.
                    For vertices = 1 points are drawn. For vertices = 2 lines are drawn.
                areas:  int, optional
                    Number of radial areas into which the image is divided.
                polygons_per_round: int, optional
                    Number of polygons which are drawn onto the image.

                Returns
                -------
                img: Individual
                    The altered image.
            """
        img = Individual(generation_no, individual_no)
        img.img = np.copy(origin_img.img)
        color = Image.get_random_color()
        max_radian_per_area = 2 * math.pi / areas
        polygons = np.zeros([areas * polygons_per_round, vertices, 2], np.int32)
        for p in range(0, polygons_per_round):
            for i in range(0, vertices):
                random_radian = random.uniform(0, max_radian_per_area)
                random_radial_coordinate = random.uniform(0, GeneticOperator.MAX_RADIAL_COORDINATE)
                for a in range(0, areas):
                    radian = a * max_radian_per_area + random_radian
                    polygons[p * areas + a][i] = Image.convert_polar_coordinates_to_point(radian,
                                                                                          random_radial_coordinate)
        cv2.fillPoly(img.img, polygons, color)
        return img

    @staticmethod
    def __color_grid(origin_img, generation_no, individual_no, columns_and_rows=COLUMNS_AND_ROWS, gradient=GRADIENT):
        """
            Lays a grid with colored tiles onto an image.

            The method takes an image (`origin_image`), copies it and adds a grid with colored tiles to it.
            The image is divided into a grid with `columns_and_rows` rows and columns. For each area a random color
            difference (RGB) is generated which will be added to each pixel of that area. The parameter `gradient`
            determines whether all tiles have a color gradient. E.g.:


                .. figure:: ../_static/_images/color_grid.png
                    :width: 200
                    :figwidth: 250

                    Example with following parameters: rows = 4, gradient = False


            Parameters
            ----------
            origin_img : Individual
                Individual which will be altered
            generation_no : int
                Number of generation, which the altered image belongs to.
            individual_no : int
                Number of individual (image) in the current generation.
            columns_and_rows: int, optional
                Number of rows and columns of the grid. Determines into how many areas the image is divided.
            gradient: bool, optional
                Determines whether each tile has a gradient.

            Returns
            -------
            img: Individual
                The altered image.
        """
        img = Individual(generation_no, individual_no)
        img.img = np.copy(origin_img.img)
        column_width = int(Image.SIZE / columns_and_rows)
        tile_method = {True: img.add_color_gradient,
                       False: img.add_color}
        mutate = tile_method[gradient]
        for i in range(0, columns_and_rows):
            for j in range(0, columns_and_rows):
                offset_x = i * column_width
                offset_y = j * column_width
                mutate(column_width, offset_x, offset_y)
        return img
