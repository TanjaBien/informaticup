from enums.dataType import DataType
from util.fileManager import FileManager as fm
from util.customWarnings import CustomWarnings
from util.validator import Validator
from main import Main
from analysis import Analysis


class Launcher:

    CONFIG_FILE = "evolution_config.json"
    _CONFIG_MODE = Validator.CONFIG_MODE
    _CONFIG_MAIN = Validator.CONFIG_MAIN
    _CONFIG_ANALYSIS = Validator.CONFIG_ANALYSIS

    def __init__(self):
        rel_path_folder = fm.get_relative_path_for_data_type(DataType.CONFIG_FILE)
        rel_path_config_file = fm.build_relative_path([rel_path_folder, Launcher.CONFIG_FILE])
        config = fm.open_json_file(rel_path_config_file)
        if Validator.validate_evolution_config_file(config):
            if config[Launcher._CONFIG_MODE] == Launcher._CONFIG_MAIN:
                main = Main()
                main.run(config)
            elif config[Launcher._CONFIG_MODE] == Launcher._CONFIG_ANALYSIS:
                analysis = Analysis()
                analysis.run(config)
            else:
                CustomWarnings.print_warning("No valid mode used in config-file. Evolution aborted.")
                return


if __name__ == '__main__':
    launcher = Launcher()
