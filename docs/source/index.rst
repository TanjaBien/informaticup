.. foolingImageGenerator documentation master file, created by
   sphinx-quickstart on Sat Dec 29 14:47:22 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to foolingImageGenerator's documentation!
=====================================
This is the documentation for the project foolingImageGenerator.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   packages
   my_modules
   
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
