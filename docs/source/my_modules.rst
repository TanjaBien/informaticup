Modules
=============
   
Core Modules
------------------
.. autosummary:: 
   :toctree: _generated/

   connection
   main
   analysis

   

Submodules
------------------
.. autosummary:: 
   :toctree: _generated/

    enums.dataType
	enums.mutation
	enums.parameter
	
	evol.evolution
	evol.generation
    evol.geneticOperator
    evol.individual
	
	gui.app
	
	util.customWarnings
	util.figure
	util.fileManager
	util.image
	util.validator