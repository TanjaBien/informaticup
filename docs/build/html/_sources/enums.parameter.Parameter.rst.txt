enums.parameter.Parameter
=========================

.. currentmodule:: enums.parameter

.. autoclass:: Parameter

   
   .. automethod:: __init__

   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~Parameter.ALLOW_DOWNSWING
      ~Parameter.AREAS
      ~Parameter.CIRCLES_PER_ROUND
      ~Parameter.COLUMNS_AND_ROWS
      ~Parameter.CONFIDENCE_THRESHOLD
      ~Parameter.DIAMETER_TO_IMAGE_SIZE
      ~Parameter.FILLED
      ~Parameter.GRADIENT
      ~Parameter.HIGH_QUALITY
      ~Parameter.MAX_GENERATION_NO
      ~Parameter.MAX_LINE_THICKNESS_FACTOR
      ~Parameter.MUTATION
      ~Parameter.PARENT_COLUMNS_AND_ROWS
      ~Parameter.PARENT_GRADIENT
      ~Parameter.PARENT_PATH
      ~Parameter.PATH
      ~Parameter.PERCENT
      ~Parameter.POLYGONS_PER_ROUND
      ~Parameter.RATIO_MAX_LINE_THICKNESS_TO_RADIUS
      ~Parameter.RECTANGLES_PER_ROUND
      ~Parameter.SHOW_EVOLUTION_PROCESS
      ~Parameter.SUCCESSORS_COUNT
      ~Parameter.VERTICES
   
   