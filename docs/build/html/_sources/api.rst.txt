API Reference
=============

.. autosummary:: 
   :toctree:

   evol
   enums
   util
   gui
   connection
   main
   analysis
