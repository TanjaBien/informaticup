enums.dataType.DataType
=======================

.. currentmodule:: enums.dataType

.. autoclass:: DataType

   
   .. automethod:: __init__

   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~DataType.CONFIG_FILE
      ~DataType.FOOLING_IMAGE
      ~DataType.HIGH_QUALITY_IMAGE
      ~DataType.IMAGE
      ~DataType.METADATA
      ~DataType.PLOT
      ~DataType.PLOT_DATA
      ~DataType.SAMPLE_IMAGE
      ~DataType.TRAFFIC_SIGN_IMAGE
   
   