# informaticup

This is a solution for the informatiCup 2019, a competition of the GI (Gesellschaft für Informatik e.V.). The task was to develop a software which is able to fool a neural network. The neural network was given by the GI and trained to recognize traffic signs. The competitors could send pictures to the neural network and as a response they would get an assessment how likely (given by the confidence) the neural network classifies the image as a certain traffic sign. The aim was to develop a program which is able to generate pictures which score a high confidence value (>= 90%) and thus are recognized as traffic sign. Those images are called fooling images.

For more information follow the [link](https://gi.de/fileadmin/GI/Hauptseite/Aktuelles/Wettbewerbe/InformatiCup/InformatiCup2019-Irrbilder.pdf).


**Attention:** The software only works with internet connection and only as long as the neural network of the GI is accessible (approximately until 30.04.19). Since the access is bound to an API Key and restricted to a certain amout of requests per minute, the software is slowed down when several people use it at the same time.


## Installation

There are 3 ways to run the software:

#### Option a:
1) Install Python3
2) Clone the repository
3) Run the following commands on the terminal:
```
pip3 install numpy requests boto3 opencv-python
pip install --no-cache-dir -r requirements.txt
```
4) Start the software via the *app.py* or the *launcher.py*

#### Option b:
1) Install Docker
2) Pull the image with following command:
```
docker pull calendula/fooling_image_generator
```
3) Configure your config files in ./foolingImageGenerator/code/config/
4) Run the docker container with following command:
```
docker run \
--volume <absoluter Pfad zur connection_config.json>:\
/app/foolingImageGenerator/config/connection_config.json \
--volume <absoluter Pfad zur evolution_config.json>:\
/app/foolingImageGenerator/config/evolution_config.json \
-it --rm fooling-image-generator
```

#### Option c:
1) Install Docker
2) Build your own Docker image with following command:
```
sudo docker build -t fooling-image-generator.
```
3) Configure your config files in ./foolingImageGenerator/code/config/
4) Run the docker container with following command:
```
docker run \
--volume <absoluter Pfad zur connection_config.json>:\
/app/foolingImageGenerator/config/connection_config.json \
--volume <absoluter Pfad zur evolution_config.json>:\
/app/foolingImageGenerator/config/evolution_config.json \
-it --rm fooling-image-generator
```

#### Option d (only for Windows):
1)  Copy the folder *windows_executable* to your machine. Make sure the target directory's path is not longer than 40 characters.
3)  Click on *app.exe*. 

## How to use the software

There are 3 different ways to use the software:
1. **App:** use a graphical interface to generate fooling images
2. **Main:** use a configuration file to generate fooling images
3. **Analysis:** use a configuration file to analyse the software

### App
If you used to *Option a* to install the software you can start the software via the *app.py*.

Windows users can also use a precompiled executable file to start the app (see *Option d*).


### Main & Analysis
Specify a configuration file or adapt the sample file  *foolingImageGenerator/config/evolution_config.json*. The *mode* distinguishes whether the program runs as *main* or *analysis*. The parameters for both modes can either be found in the documentation of the code, the html-documentation (referred in the next paragraph) or in the *InformatiCup.pdf* (in German).

When using docker you must specify the path to the configuration file when executing the command. If you used *Option a* for installing you have to place your configuration file exactly where the sample file is located and also use the same file name (*"evolution_config.json"*).


### Documentation
A comprehensive documentation can be found in the folder *./docs*. The html view can be opened with the *./docs/build/html/index.html* file.

There is an additional documentation (in german) which includes the theoretical fundamentals and details to the implementation: *InformatiCup.pdf*

